#!/bin/env python

'''Check strippping log files for warnings about duplicate candidates.'''

import subprocess

excludes = ('Jet', '_WG_', 'Input', 'suppressed')
def check_duplicates(fname, excludes = excludes) :
    finds = {}
    with open(fname) as f :
        line = f.readline()
        while line :
            if not 'WARNING PrintDuplicates' in line or any(excl in line for excl in excludes) :
                line = f.readline()
                continue
            istart = line.find("'")
            iend = line.find("'", istart+1)
            if iend == -1 :
                print "Couldn't understand line\n" + line + "in file " + fname
                line = f.readline()
                continue                
            location = line[istart+1:iend]
            info = ''
            line = f.readline()
            while line.startswith('ToolSvc.PrintDuplicateDecays') :
                info += line
                line = f.readline()
            if not info :
                print 'No info for location ' + location + ' in file ' + fname
            if not location in finds :
                finds[location] = []
            finds[location].append(info)
    return finds

def check_all_duplicates(dirnames, excludes = excludes, fname = 'stdout') :
    if isinstance(dirnames, str) :
        dirnames = (dirnames,)

    allfinds = {}
    for dirname in dirnames :
        proc = subprocess.Popen(['find', dirname, '-name', fname],
                                stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        stdout, stderr = proc.communicate()
        for _fname in filter(None, stdout.split('\n')) :
            finds = check_duplicates(_fname, excludes)
            for find, infos in finds.items() :
                if find in allfinds :
                    allfinds[find] += infos
                else :
                    allfinds[find] = infos
    return allfinds

if __name__ == '__main__' :
    import sys, argparse
    argparser = argparse.ArgumentParser()
    argparser.add_argument('dirnames', nargs = '*', help = 'Directories to search for stdout files to parse.')
    args = argparser.parse_args()

    finds = check_all_duplicates(args.dirnames)
    for location, infos in finds.items() :
        print '*' * 20, location, '*' * 20
        for info in infos :
            print info
            print
        print

