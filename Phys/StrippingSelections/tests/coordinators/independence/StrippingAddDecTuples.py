from Gaudi.Configuration import appendPostConfigAction
from Configurables import DaVinci, Stripping, EventTuple, TupleToolStripping

def make_tuples() :
    evttuple = EventTuple('StrippingDecsTuple')
    evttuple.ToolList = ['TupleToolStripping', 'TupleToolEventInfo']
    ttstrip = evttuple.addTool(TupleToolStripping)
    linenames = set()
    for stream in Stripping().cache['streams'] :
        linenames.add('StrippingStream' + stream.name())
        for line in stream.lines :
            linenames.add(line.name())
    ttstrip.StrippingList = [name + 'Decision' for name in linenames]
    print 'Adding TupleToolStripping for:'
    print ttstrip.StrippingList
    ttstrip.StrippingReportsLocations = "Strip/Phys/DecReports"
    DaVinci().appendToMainSequence([evttuple])

DaVinci().TupleFile = 'DaVinciTuples.root'
appendPostConfigAction(make_tuples)
