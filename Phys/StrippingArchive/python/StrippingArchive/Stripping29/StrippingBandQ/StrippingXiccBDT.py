'''
Module for the Doublely Heavy Flavour Baryons, Xicc,  
Note:
*. CPU-intensive cuts like IPCHI2 are not re-applied
   if being identical to those in the common particles

Include the following lines:
Xicc++ -> Lc+ pi+
Xicc++ -> Lc+ K- pi+ pi+
Xicc++ -> D+ p+
Xicc++ -> D+ p+ K- pi+
Xicc++ -> D0 p+ K- pi+ pi+
---------------------------
Xicc+  -> Lc+ K- pi+ 
Xicc+  -> D0 p+
Xicc+  -> D0 p+ K- pi+
Xicc+  -> D+ p+ K- 
'''

__author__=['Jibo He']
__date__ = '20/11/2016'
__version__= '$Revision: 1.0 $'


__all__ = (
    'XiccBDTConf',
    'default_config'
    )

default_config = {
    'NAME'              :  'XiccBDT',
    'BUILDERTYPE'       :  'XiccBDTConf',
    'CONFIG'    : {
        'PionCuts'      : "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",
        'KaonCuts'      : "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",
        'ProtonCuts'    : "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",

        'DplusCuts'     : "(ADMASS('D+')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>100)",
        'D0Cuts'        : "(ADMASS('D0')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>64)",
    
        'LcComCuts'     : "(APT>1.0*GeV) & ((ADAMASS('Lambda_c+')<50*MeV) | (ADAMASS('Xi_c+')<50*MeV)) & (ADOCACHI2CUT(30, ''))",
        'LcMomCuts'     : "((ADMASS('Lambda_c+')<20*MeV) | (ADMASS('Xi_c+')<20*MeV)) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVVDCHI2>16)",
        
        'UnPionCuts'    : "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4)",  
        'UnKaonCuts'    : "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4)",
        'UnProtonCuts'  : "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4)",  

        'TightPionCuts'   : "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)",
        'TightKaonCuts'   : "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)",
        'TightProtonCuts' : "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)",
         
        'XiccComCuts'   : "(AM<4.6*GeV) & (APT>2*GeV)",
        'XiccMomCuts'   : "(M<4.4*GeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.99) & (BPVIPCHI2()<25)",

        'XiccPP2LcPiMVACut'   :  "0.18",
        'XiccPP2LcPiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccPP2LcPi_BDT_v1r0.xml',

        'XiccPP2DpMVACut'   :  "0.05",
        'XiccPP2DpXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccPP2Dp_BDT_v1r0.xml',

        'XiccPP2DpKpiMVACut'   :  "-0.05",
        'XiccPP2DpKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccPP2DpKpi_BDT_v1r0.xml',

        'XiccPP2D0pKpipiMVACut'   :  "-0.1",
        'XiccPP2D0pKpipiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccPP2D0pKpipi_BDT_v1r0.xml',
        
        'XiccPP2LcKpipiMVACut'   :  "0.",
        'XiccPP2LcKpipiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccPP2LcKpipi_BDT_v1r0.xml',

        'XiccP2LcKpiMVACut'   :  "0.16",
        'XiccP2LcKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccP2LcKpi_BDT_v1r0.xml',

        'XiccP2D0pMVACut'   :  "0.1",
        'XiccP2D0pXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccP2D0p_BDT_v1r0.xml',

        'XiccP2D0pKpiMVACut'   :  "-0.05",
        'XiccP2D0pKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccP2D0pKpi_BDT_v1r0.xml',

        'XiccP2DpKMVACut'   :  "0.",
        'XiccP2DpKXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccP2DpK_BDT_v1r0.xml',       
        },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['BandQ']
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays
from PhysSelPython.Wrappers import Selection, MergedSelection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

class XiccBDTConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config
                
        """
        Basic particles, long tracks
        """
        from StandardParticles import StdAllLooseANNPions, StdAllLooseANNKaons, StdAllLooseANNProtons
        
        self.SelPions = self.createSubSel( OutputList = self.name + "SelPions",
                                           InputList =  StdAllLooseANNPions , 
                                           Cuts = config['PionCuts']
                                           )
        
        self.SelKaons = self.createSubSel( OutputList = self.name + "SelKaons",
                                           InputList = StdAllLooseANNKaons, 
                                           Cuts = config['KaonCuts']
                                           )
                                                   
        self.SelProtons = self.createSubSel( OutputList = self.name + "SelProtons",
                                             InputList = StdAllLooseANNProtons, 
                                             Cuts = config['ProtonCuts']
                                             )

        #from StandardParticles import StdAllLooseANNPions
        self.SelUnPions = self.createSubSel( OutputList = self.name + "SelUnPions",
                                               InputList =  StdAllLooseANNPions , 
                                               Cuts = config['UnPionCuts']
                                               )
        
        self.SelUnKaons = self.createSubSel( OutputList = self.name + "SelUnKaons",
                                               InputList =  StdAllLooseANNKaons , 
                                               Cuts = config['UnKaonCuts']
                                               )
        self.SelUnProtons = self.createSubSel( OutputList = self.name + "SelUnProtons",
                                                 InputList =  StdAllLooseANNProtons , 
                                                 Cuts = config['UnProtonCuts']
                                                 )
        
        self.SelTightPions = self.createSubSel( OutputList = self.name + "SelTightPions",
                                                    InputList =  StdAllLooseANNPions , 
                                                    Cuts = config['TightPionCuts']
                                                    )
        
        self.SelTightKaons = self.createSubSel( OutputList = self.name + "SelTightKaons",
                                                    InputList =  StdAllLooseANNKaons , 
                                                    Cuts = config['TightKaonCuts']
                                                    )
        
        
        """
        Dplus->K pi pi 
        """
        self.SelDplus = self.createSubSel( OutputList = self.name + "SelDplus",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseDplus2KPiPi/Particles' ), 
                                           Cuts = config['DplusCuts']
                                           )

        """
        D0->K pi
        """
        self.SelD0 = self.createSubSel( OutputList = self.name + "SelD0",
                                        InputList =  DataOnDemand(Location = 'Phys/StdLooseD02KPi/Particles' ), 
                                        Cuts = config['D0Cuts']
                                        )
        
        """
        LambdaC->p K pi
        """
        self.SelLambdaC = self.createCombinationSel( OutputList = self.name + "SelLambdaC",
                                                     DecayDescriptor = "[ Lambda_c+ -> p+  K-  pi+ ]cc",
                                                     DaughterLists = [ self.SelProtons,
                                                                       self.SelKaons,
                                                                       self.SelPions ],                    
                                                     PreVertexCuts  = config['LcComCuts'],
                                                     PostVertexCuts = config['LcMomCuts'] )
        
        

        #
        # Stripping line 
        #
        """
        -------------------------------------------
        Xicc++
        -------------------------------------------

        """
        self.XiccPP2LcPiVars = {
            "sqrt(LcP_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(LcPi_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 3))",
            "sqrt(LcK_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(Lc_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XiccPi_IPCHI2_OWNPV)"  : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(LcP_PT)"                : "log(CHILD(PT, 1, 1))", 
            "log(LcPi_PT)"               : "log(CHILD(PT, 1, 3))", 
            "log(LcK_PT)"                : "log(CHILD(PT, 1, 2))", 
            "log(Lc_PT)"                 : "log(CHILD(PT, 1))",
            "log(XiccPi_PT)"             : "log(CHILD(PT, 2))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(Lc_FDCHI2_OWNPV)"      : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }
        
        
        """
        XiccPP2LcPi
        """
        self.SelXiccPP2LcPi = self.createCombinationSel( OutputList = self.name + "SelXiccPP2LcPi",
                                                         DecayDescriptor = "[ Xi_cc++ -> Lambda_c+ pi+ ]cc",
                                                         DaughterLists = [ self.SelLambdaC,
                                                                           self.SelUnPions ],
                                                         PreVertexCuts  = config['XiccComCuts'],
                                                         PostVertexCuts = config['XiccMomCuts'] )

        
        self.MvaXiccPP2LcPi = self.applyMVA( self.name + "MvaXiccPP2LcPi",
                                           SelB        = self.SelXiccPP2LcPi,
                                           MVAVars     = self.XiccPP2LcPiVars,
                                           MVACutValue = config['XiccPP2LcPiMVACut'], 
                                           MVAxmlFile  = config['XiccPP2LcPiXmlFile']
                                           )
        
        self.XiccPP2LcPiLine = StrippingLine( self.name + '_XiccPP2LcPiLine',                                               
                                            algos     = [ self.MvaXiccPP2LcPi ],
                                            MDSTFlag  = False
                                            )
        
        self.registerLine( self.XiccPP2LcPiLine )


        
        """
        XiccPP2LcKpipi
        """       
        self.XiccPP2LcKpipiVars = {
            "sqrt(LcP_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(LcPi_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 3))",
            "sqrt(LcK_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(Lc_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XiccPi1_IPCHI2_OWNPV)" : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(XiccPi2_IPCHI2_OWNPV)" : "sqrt(CHILD(MIPCHI2DV(), 4 ))",
            "sqrt(XiccK_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(LcP_PT)"                : "log(CHILD(PT, 1, 1))", 
            "log(LcPi_PT)"               : "log(CHILD(PT, 1, 3))", 
            "log(LcK_PT)"                : "log(CHILD(PT, 1, 2))", 
            "log(Lc_PT)"                 : "log(CHILD(PT, 1))",   
            "log(XiccPi1_PT)"            : "log(CHILD(PT, 3))",
            "log(XiccPi2_PT)"            : "log(CHILD(PT, 4))",
            "log(XiccK_PT)"              : "log(CHILD(PT, 2))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(Lc_FDCHI2_OWNPV)"      : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }
        
        self.SelXiccPP2LcKpipi = self.createCombinationSel( OutputList = self.name + "SelXiccPP2LcKpipi",
                                                            DecayDescriptor = "[ Xi_cc++ -> Lambda_c+ K- pi+ pi+ ]cc",
                                                            DaughterLists = [ self.SelLambdaC,
                                                                              self.SelTightKaons,
                                                                              self.SelTightPions ],
                                                            PreVertexCuts  = config['XiccComCuts'],
                                                            PostVertexCuts = config['XiccMomCuts'] )
        
        self.MvaXiccPP2LcKpipi = self.applyMVA( self.name + "MvaXiccPP2LcKpipi",
                                           SelB        = self.SelXiccPP2LcKpipi,
                                           MVAVars     = self.XiccPP2LcKpipiVars,
                                           MVACutValue = config['XiccPP2LcKpipiMVACut'], 
                                           MVAxmlFile  = config['XiccPP2LcKpipiXmlFile']
                                           )
        
        self.XiccPP2LcKpipiLine = StrippingLine( self.name + '_XiccPP2LcKpipiLine',                                               
                                                 algos     = [ self.MvaXiccPP2LcKpipi ],
                                                 MDSTFlag  = False
                                                 )
        
        self.registerLine( self.XiccPP2LcKpipiLine )


        """
        XiccPP2Dp
        """
        self.XiccPP2DpVars = {
            "sqrt(DK_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(DPi1_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(DPi2_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 3))",
            "sqrt(D_IPCHI2_OWNPV)"       : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XiccP_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(DK_PT)"                 : "log(CHILD(PT, 1, 1))", 
            "log(DPi1_PT)"               : "log(CHILD(PT, 1, 2))", 
            "log(DPi2_PT)"               : "log(CHILD(PT, 1, 3))", 
            "log(D_PT)"                  : "log(CHILD(PT, 1))",   
            "log(XiccP_PT)"              : "log(CHILD(PT, 2))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(D_FDCHI2_OWNPV)"       : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }
        
        self.SelXiccPP2Dp = self.createCombinationSel( OutputList = self.name + "SelXiccPP2Dp",
                                                       DecayDescriptor = "[ Xi_cc++ -> D+ p+ ]cc",
                                                       DaughterLists = [ self.SelDplus,
                                                                         self.SelUnProtons,
                                                                         ],
                                                       PreVertexCuts  = config['XiccComCuts'],
                                                       PostVertexCuts = config['XiccMomCuts'] )
        

        self.MvaXiccPP2Dp = self.applyMVA( self.name + "MvaXiccPP2Dp",
                                           SelB        = self.SelXiccPP2Dp,
                                           MVAVars     = self.XiccPP2DpVars,
                                           MVACutValue = config['XiccPP2DpMVACut'], 
                                           MVAxmlFile  = config['XiccPP2DpXmlFile']
                                           )

        self.XiccPP2DpLine = StrippingLine( self.name + '_XiccPP2DpLine',                                               
                                            algos     = [ self.MvaXiccPP2Dp ],
                                            MDSTFlag  = False
                                            )
        
        self.registerLine( self.XiccPP2DpLine )

        
        """
        XiccPP2DpKpi
        """
        self.XiccPP2DpKpiVars = {
            "sqrt(DK_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(DPi1_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(DPi2_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 3))",
            "sqrt(D_IPCHI2_OWNPV)"       : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XiccP_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(XiccK_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(XiccPi_IPCHI2_OWNPV)"  : "sqrt(CHILD(MIPCHI2DV(), 4 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(DK_PT)"                 : "log(CHILD(PT, 1, 1))", 
            "log(DPi1_PT)"               : "log(CHILD(PT, 1, 2))", 
            "log(DPi2_PT)"               : "log(CHILD(PT, 1, 3))", 
            "log(D_PT)"                  : "log(CHILD(PT, 1))",   
            "log(XiccP_PT)"              : "log(CHILD(PT, 2))",
            "log(XiccK_PT)"              : "log(CHILD(PT, 3))",
            "log(XiccPi_PT)"             : "log(CHILD(PT, 4))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(D_FDCHI2_OWNPV)"       : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }
        
        self.SelXiccPP2DpKpi = self.createCombinationSel( OutputList = self.name + "SelXiccPP2DpKpi",
                                                          DecayDescriptor = "[ Xi_cc++ -> D+ p+ K- pi+ ]cc",
                                                          DaughterLists = [ self.SelDplus,
                                                                            self.SelUnProtons,
                                                                            self.SelUnKaons,
                                                                            self.SelUnPions ],
                                                          PreVertexCuts  = config['XiccComCuts'],
                                                          PostVertexCuts = config['XiccMomCuts'] )
        
        
        self.MvaXiccPP2DpKpi = self.applyMVA( self.name + "MvaXiccPP2DpKpi",
                                           SelB        = self.SelXiccPP2DpKpi,
                                           MVAVars     = self.XiccPP2DpKpiVars,
                                           MVACutValue = config['XiccPP2DpKpiMVACut'], 
                                           MVAxmlFile  = config['XiccPP2DpKpiXmlFile']
                                           )
        
        self.XiccPP2DpKpiLine = StrippingLine( self.name + '_XiccPP2DpKpiLine',                                               
                                                 algos     = [ self.MvaXiccPP2DpKpi ],
                                                 MDSTFlag  = False
                                               )
        
        self.registerLine( self.XiccPP2DpKpiLine )

        
        """
        XiccPP2D0pKpipi
        """        
        self.XiccPP2D0pKpipiVars = {
            "sqrt(DK_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(DPi_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(D_IPCHI2_OWNPV)"       : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XiccP_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(XiccK_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(XiccPi1_IPCHI2_OWNPV)" : "sqrt(CHILD(MIPCHI2DV(), 4 ))",
            "sqrt(XiccPi2_IPCHI2_OWNPV)" : "sqrt(CHILD(MIPCHI2DV(), 5 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(DK_PT)"                 : "log(CHILD(PT, 1, 1))", 
            "log(DPi_PT)"                : "log(CHILD(PT, 1, 2))", 
            "log(D_PT)"                  : "log(CHILD(PT, 1))",   
            "log(XiccP_PT)"              : "log(CHILD(PT, 2))",
            "log(XiccK_PT)"              : "log(CHILD(PT, 3))",
            "log(XiccPi1_PT)"            : "log(CHILD(PT, 4))",
            "log(XiccPi2_PT)"            : "log(CHILD(PT, 5))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(D_FDCHI2_OWNPV)"       : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }
        
        self.SelXiccPP2D0pKpipi = self.createCombinationSel( OutputList = self.name + "SelXiccPP2D0pKpipi",
                                                          DecayDescriptor = "[ Xi_cc++ -> D0 p+ K- pi+ pi+ ]cc",
                                                          DaughterLists = [ self.SelD0,
                                                                            self.SelUnProtons,
                                                                            self.SelUnKaons,
                                                                            self.SelPions ],  # w/ IPchi2 cut to speed up
                                                          PreVertexCuts  = config['XiccComCuts'],
                                                          PostVertexCuts = config['XiccMomCuts'] )
        
        self.MvaXiccPP2D0pKpipi = self.applyMVA( self.name + "MvaXiccPP2D0pKpipi",
                                           SelB        = self.SelXiccPP2D0pKpipi,
                                           MVAVars     = self.XiccPP2D0pKpipiVars,
                                           MVACutValue = config['XiccPP2D0pKpipiMVACut'], 
                                           MVAxmlFile  = config['XiccPP2D0pKpipiXmlFile']
                                           )
        
        self.XiccPP2D0pKpipiLine = StrippingLine( self.name + '_XiccPP2D0pKpipiLine',                                               
                                                 algos     = [ self.MvaXiccPP2D0pKpipi ],
                                                 MDSTFlag  = False
                                                  )
        
        self.registerLine( self.XiccPP2D0pKpipiLine )



        """
        -------------------------------------------
        Xicc+
        -------------------------------------------

        """

        """
        XiccP2LcKpi
        """
        self.XiccP2LcKpiVars = {
            "sqrt(LcP_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(LcPi_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 3))",
            "sqrt(LcK_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(Lc_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XiccPi_IPCHI2_OWNPV)"  : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(XiccK_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(LcP_PT)"                : "log(CHILD(PT, 1, 1))", 
            "log(LcPi_PT)"               : "log(CHILD(PT, 1, 3))", 
            "log(LcK_PT)"                : "log(CHILD(PT, 1, 2))", 
            "log(Lc_PT)"                 : "log(CHILD(PT, 1))",   
            "log(XiccPi_PT)"             : "log(CHILD(PT, 3))",
            "log(XiccK_PT)"              : "log(CHILD(PT, 2))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(Lc_FDCHI2_OWNPV)"      : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }
         
         
        self.SelXiccP2LcKpi = self.createCombinationSel( OutputList = self.name + "SelXiccP2LcKpi",
                                                         DecayDescriptor = "[ Xi_cc+ -> Lambda_c+ K- pi+ ]cc",
                                                         DaughterLists = [ self.SelLambdaC,
                                                                           self.SelUnKaons,
                                                                           self.SelUnPions ],
                                                         PreVertexCuts  = config['XiccComCuts'],
                                                         PostVertexCuts = config['XiccMomCuts'] )
        
        self.MvaXiccP2LcKpi = self.applyMVA( self.name + "MvaXiccP2LcKpi",
                                           SelB        = self.SelXiccP2LcKpi,
                                           MVAVars     = self.XiccP2LcKpiVars,
                                           MVACutValue = config['XiccP2LcKpiMVACut'], 
                                           MVAxmlFile  = config['XiccP2LcKpiXmlFile']
                                           )
       
        self.XiccP2LcKpiLine = StrippingLine( self.name + '_XiccP2LcKpiLine',                                               
                                                 algos     = [ self.MvaXiccP2LcKpi ],
                                                 MDSTFlag  = False
                                                 )
        
        self.registerLine( self.XiccP2LcKpiLine )

        
        """
        XiccP2D0p
        """
        self.XiccP2D0pVars = {
            "sqrt(DK_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(DPi_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(D_IPCHI2_OWNPV)"       : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XiccP_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(DK_PT)"                 : "log(CHILD(PT, 1, 1))", 
            "log(DPi_PT)"                : "log(CHILD(PT, 1, 2))", 
            "log(D_PT)"                  : "log(CHILD(PT, 1))",   
            "log(XiccP_PT)"              : "log(CHILD(PT, 2))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(D_FDCHI2_OWNPV)"       : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }
        
        self.SelXiccP2D0p = self.createCombinationSel( OutputList = self.name + "SelXiccP2D0p",
                                                       DecayDescriptor = "[ Xi_cc+ -> D0 p+ ]cc",
                                                       DaughterLists = [ self.SelD0,
                                                                         self.SelUnProtons,
                                                                         ],
                                                       PreVertexCuts  = config['XiccComCuts'],
                                                       PostVertexCuts = config['XiccMomCuts'] )
        
       
        self.MvaXiccP2D0p = self.applyMVA( self.name + "MvaXiccP2D0p",
                                           SelB        = self.SelXiccP2D0p,
                                           MVAVars     = self.XiccP2D0pVars,
                                           MVACutValue = config['XiccP2D0pMVACut'], 
                                           MVAxmlFile  = config['XiccP2D0pXmlFile']
                                           )
    
        self.XiccP2D0pLine = StrippingLine( self.name + '_XiccP2D0pLine',                                               
                                            algos     = [ self.MvaXiccP2D0p ],
                                            MDSTFlag  = False
                                            )
        
        self.registerLine( self.XiccP2D0pLine )        


        """
        XiccP2D0pKpi
        """
        self.XiccP2D0pKpiVars = {
            "sqrt(DK_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(DPi_IPCHI2_OWNPV)"     : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(D_IPCHI2_OWNPV)"       : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XiccP_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(XiccK_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(XiccPi_IPCHI2_OWNPV)"  : "sqrt(CHILD(MIPCHI2DV(), 4 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(DK_PT)"                 : "log(CHILD(PT, 1, 1))", 
            "log(DPi_PT)"                : "log(CHILD(PT, 1, 2))", 
            "log(D_PT)"                  : "log(CHILD(PT, 1))",   
            "log(XiccP_PT)"              : "log(CHILD(PT, 2))",
            "log(XiccK_PT)"              : "log(CHILD(PT, 3))",
            "log(XiccPi_PT)"             : "log(CHILD(PT, 4))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(D_FDCHI2_OWNPV)"       : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }
                
        self.SelXiccP2D0pKpi = self.createCombinationSel( OutputList = self.name + "SelXiccP2D0pKpi",
                                                          DecayDescriptor = "[ Xi_cc+ -> D0 p+ K- pi+ ]cc",
                                                          DaughterLists = [ self.SelD0,
                                                                            self.SelUnProtons,
                                                                            self.SelUnKaons,
                                                                            self.SelUnPions ],  
                                                          PreVertexCuts  = config['XiccComCuts'],
                                                          PostVertexCuts = config['XiccMomCuts'] )
        
        
        self.MvaXiccP2D0pKpi = self.applyMVA( self.name + "MvaXiccP2D0pKpi",
                                           SelB        = self.SelXiccP2D0pKpi,
                                           MVAVars     = self.XiccP2D0pKpiVars,
                                           MVACutValue = config['XiccP2D0pKpiMVACut'], 
                                           MVAxmlFile  = config['XiccP2D0pKpiXmlFile']
                                           )
        
        self.XiccP2D0pKpiLine = StrippingLine( self.name + '_XiccP2D0pKpiLine',                                               
                                                 algos     = [ self.MvaXiccP2D0pKpi ],
                                                 MDSTFlag  = False
                                               )
        
        self.registerLine( self.XiccP2D0pKpiLine )

        
        """
        XiccP2DpK
        """
        self.XiccP2DpKVars = {
            "sqrt(DK_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1, 1))",
            "sqrt(DPi1_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 2))",
            "sqrt(DPi2_IPCHI2_OWNPV)"    : "sqrt(CHILD(MIPCHI2DV(), 1, 3))",
            "sqrt(D_IPCHI2_OWNPV)"       : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(XiccP_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(XiccK_IPCHI2_OWNPV)"   : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(C_IPCHI2_OWNPV)"       : "sqrt(BPVIPCHI2())" ,
            "log(DK_PT)"                 : "log(CHILD(PT, 1, 1))", 
            "log(DPi1_PT)"               : "log(CHILD(PT, 1, 2))", 
            "log(DPi2_PT)"               : "log(CHILD(PT, 1, 3))", 
            "log(D_PT)"                  : "log(CHILD(PT, 1))",   
            "log(XiccP_PT)"              : "log(CHILD(PT, 2))",
            "log(XiccK_PT)"              : "log(CHILD(PT, 3))",
            "log(C_PT)"                  : "log(PT)",
            "sqrt(D_FDCHI2_OWNPV)"       : "sqrt(CHILD(BPVVDCHI2,1))",
            "sqrt(C_FDCHI2_OWNPV)"       : "sqrt(BPVVDCHI2)" ,
            "C_DIRA_OWNPV"               : "BPVDIRA"
            }
        self.SelXiccP2DpK = self.createCombinationSel( OutputList = self.name + "SelXiccP2DpK",
                                                          DecayDescriptor = "[ Xi_cc+ -> D+ p+ K- ]cc",
                                                          DaughterLists = [ self.SelDplus,
                                                                            self.SelUnProtons,
                                                                            self.SelUnKaons ], 
                                                          PreVertexCuts  = config['XiccComCuts'],
                                                          PostVertexCuts = config['XiccMomCuts'] )
        
        
        self.MvaXiccP2DpK = self.applyMVA( self.name + "MvaXiccP2DpK",
                                           SelB        = self.SelXiccP2DpK,
                                           MVAVars     = self.XiccP2DpKVars,
                                           MVACutValue = config['XiccP2DpKMVACut'], 
                                           MVAxmlFile  = config['XiccP2DpKXmlFile']
                                           )
        
        self.XiccP2DpKLine = StrippingLine( self.name + '_XiccP2DpKLine',                                               
                                                 algos     = [ self.MvaXiccP2DpK ],
                                                 MDSTFlag  = False
                                            )
        
        self.registerLine( self.XiccP2DpKLine )




    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)

    def applyMVA( self, name, 
                  SelB,
                  MVAVars,
                  MVAxmlFile,
                  MVACutValue
                  ):
        from MVADictHelpers import addTMVAclassifierValue
        from Configurables import FilterDesktop as MVAFilterDesktop

        _FilterB = MVAFilterDesktop( name + "Filter",
                                     Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue  )

        addTMVAclassifierValue( Component = _FilterB,
                                XMLFile   = MVAxmlFile,
                                Variables = MVAVars,
                                ToolName  = name )
        
        return Selection( name,
                          Algorithm =  _FilterB,
                          RequiredSelections = [ SelB ] )
