
__author__ = ['Carla Marin']
__date__ = '23/11/2016'
__version__ = '$1.1 $'

'''
    stripping code for Kshort --> eePiPi
'''

default_config =  {
    'NAME'        : 'Kshort2eePiPi',
    'WGs'         : ['RD'],
    'BUILDERTYPE' : 'Kshort2eePiPiConf',
    'CONFIG'      : {   'Kshort2eePiPi_eeFromTracksLinePrescale'        : 1 ,
                        'Kshort2eePiPi_eeFromTracksLinePostscale'       : 1 ,
                        'Kshort2eePiPi_eeFromTracks_TOSLinePrescale'    : 1 ,
                        'Kshort2eePiPi_eeFromTracks_TOSLinePostscale'   : 1 ,
                        
                        'ePT'           : 100.          , #MeV
                        'eMINIPCHI2'    : 16            , #adimensional
                        'ePIDe'         : -4            , #adimensional
                        'eGhostProb'    : 0.5           , #adimensional

                        'PionPT'        : 100           , #MeV
                        'PionMINIPCHI2' : 16            , #adimensional
                        'PionPIDK'      : 5             , #adimensional
                        'PionGhostProb' : 0.5           , #adimensional
    
                        #4body
                        'KsMAXDOCA'     : 1.            , #mm
                        'KsLifetime'    : 0.01*89.53    , #0.01*10^-12s
                        'KsIP'          : 1             , #mm
                        'MaxKsMass'     : 800.          , #MeV, comb mass high limit
                        'KsVtxChi2'     : 50            ,

                        'eMINIPCHI2_soft': 2             , #adimensional
                        'ePIDe_soft'     : -8            , #adimensional
                        'MaxKsMass_soft' : 950.          , #MeV, comb mass high limit
                        
                        'TISTOSDict'     : {'L0(Electron|Hadron|Muon|DiMuon|Photon)Decision%TOS': 0,
                                            'Hlt1(Two)?TrackMVA.*Decision%TOS'    : 0,
                                            'Hlt1SingleElectronNoIPDecision%TOS'  : 0,
                                            'Hlt1DiMuonLowMassDecision%TOS'       : 0,
                                            },
                    },
    'STREAMS'     : ['Leptonic']
    }

__all__ = ('Kshort2eePiPiConf',
           'default_config'
           )

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays as Combine3Particles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllNoPIDsPions
from StandardParticles import StdDiElectronFromTracks

class Kshort2eePiPiConf(LineBuilder) :
    """
    Builder for Kshort --> pi,pi,e,e
    """
    
    __configuration_keys__ = ('ePT',
                              'eMINIPCHI2',
                              'ePIDe',
                              'eGhostProb',
                                                            
                              'PionPT',
                              'PionMINIPCHI2',
                              'PionGhostProb',
                              'PionPIDK',
                              
                              #4body
                              'KsMAXDOCA',
                              'KsLifetime',
                              'KsIP',
                              'MaxKsMass',
                              'KsVtxChi2',
                              
                              # TOS line
                              'eMINIPCHI2_soft',
                              'ePIDe_soft',
                              'MaxKsMass_soft',
                              'TISTOSDict',

                              'Kshort2eePiPi_eeFromTracksLinePrescale',
                              'Kshort2eePiPi_eeFromTracksLinePostscale',
                              'Kshort2eePiPi_eeFromTracks_TOSLinePrescale',
                              'Kshort2eePiPi_eeFromTracks_TOSLinePostscale',
                              )
                              
    def __init__(self, name, config):

        self.name = name
        LineBuilder.__init__(self, name, config)
        
        # 1 : Make e's for Kshort2eePiPi
        selElecsFromTracksForeePiPi = makeElecsFromTracksForeePiPi("ElecsFromTracksFor"+self.name,
                                                                   StdDiElectronFromTracks,
                                                                   config["ePT"],
                                                                   config["eMINIPCHI2"],
                                                                   config["eGhostProb"],
                                                                   config["ePIDe"]
                                                                   )

        selElecsFromTracksForeePiPiSoft = makeElecsFromTracksForeePiPi("ElecsFromTracksFor"+self.name+"_soft",
                                                                       StdDiElectronFromTracks,
                                                                       config["ePT"],
                                                                       config["eMINIPCHI2_soft"],
                                                                       config["eGhostProb"],
                                                                       config["ePIDe_soft"]
                                                                      )

        # 2 : Make Pions for Kshort2eePiPi
        selPionsForeePiPi = makePionsForeePiPi("PionsFor"+self.name,
                                               StdAllNoPIDsPions,
                                               config["PionPT"],
                                               config["PionMINIPCHI2"],
                                               config["PionGhostProb"],
                                               config["PionPIDK"]
                                               )
        
        # 3 : Combine
        selKshort2eePiPiFromTracks = self._makeKshort2eePiPi(self.name+"_eeFromTracks",
                                                             "KS0 -> pi+ pi- J/psi(1S)",
                                                             selPionsForeePiPi,
                                                             selElecsFromTracksForeePiPi,
                                                             config["MaxKsMass"],
                                                             config["KsMAXDOCA"],
                                                             config["KsIP"],
                                                             config["KsLifetime"],
                                                             config["KsVtxChi2"]
                                                             )
 
        selKshort2eePiPiFromTracks_forTOS = self._makeKshort2eePiPi(self.name+"_eeFromTracks"+"_forTOS",
                                                                    "KS0 -> pi+ pi- J/psi(1S)",
                                                                    selPionsForeePiPi,
                                                                    selElecsFromTracksForeePiPiSoft,
                                                                    config["MaxKsMass_soft"],
                                                                    config["KsMAXDOCA"],
                                                                    config["KsIP"],
                                                                    config["KsLifetime"],
                                                                    config["KsVtxChi2"]
                                                                    )

        # 3b : trigger filter
        selKshort2eePiPiFromTracks_TOS = tisTosSelection(self.name+"_eeFromTracks"+"_TOS",
                                                         selKshort2eePiPiFromTracks_forTOS,
                                                         config['TISTOSDict'])
        # 4 : Declare Lines
        self.eePiPiFromTracksLine = StrippingLine(self.name+"_eeFromTracks"+"Line",
                                                  prescale  = config['Kshort2eePiPi_eeFromTracksLinePrescale'],
                                                  postscale = config['Kshort2eePiPi_eeFromTracksLinePostscale'],
                                                  selection = selKshort2eePiPiFromTracks,
                                                  MDSTFlag  = False
                                                  )

        self.eePiPiFromTracksTOSLine = StrippingLine(self.name+"_eeFromTracks"+"_TOSLine",
                                                     prescale  = config['Kshort2eePiPi_eeFromTracks_TOSLinePrescale'],
                                                     postscale = config['Kshort2eePiPi_eeFromTracks_TOSLinePostscale'],
                                                     selection = selKshort2eePiPiFromTracks_TOS,
                                                     MDSTFlag  = False
                                                     )

        # 5 : register Line
        self.registerLine( self.eePiPiFromTracksLine )
        self.registerLine( self.eePiPiFromTracksTOSLine )
    
#####################################################
    def _makeKshort2eePiPi(self, name, decay, pionSel, elecSel, MaxKsMass, KsMAXDOCA, KsIP, KsLifetime, KsVtxChi2):
        """
        Handy interface for Kshort2eePiPi
        """
        return makeKshort2eePiPi(name,
                                 decay,
                                 pionSel,
                                 elecSel,
                                 MaxKsMass,
                                 KsMAXDOCA,
                                 KsIP,
                                 KsLifetime,
                                 KsVtxChi2
                                 )

#####################################################
def makeKshort2eePiPi(name, decay, pionSel, elecSel, MaxKsMass, KsMAXDOCA, KsIP, KsLifetime, KsVtxChi2):
    """
    Makes the KS0 -> pi+ pi- (J/psi(1S) -> e+ e-)
    """

    _comb12cut =    "(AMAXDOCA('') < %(KsMAXDOCA)s *mm)" % locals()
    
    _combcut =      "(AM < %(MaxKsMass)s *MeV) & "\
                    "(AMAXDOCA('') < %(KsMAXDOCA)s *mm)" % locals()
    
    
    _mothercut =    "(M < %(MaxKsMass)s *MeV) &"\
                    "(MIPDV(PRIMARY) < %(KsIP)s *mm) & "\
                    "((BPVVDSIGN*M/P) > %(KsLifetime)s*2.9979e-01) & "\
                    "(VFASPF(VCHI2/VDOF) < %(KsVtxChi2)s) " % locals()

    _Combine = Combine3Particles(DecayDescriptor=decay,
                                 Combination12Cut=_comb12cut,
                                 CombinationCut=_combcut,
                                 MotherCut=_mothercut
                                 )

    return Selection(name,
                     Algorithm=_Combine,
                     RequiredSelections=[elecSel,pionSel]
                     )

#####################################################
def makeElecsFromTracksForeePiPi(name, elecs, ePT, eMINIPCHI2, eGhostProb, ePIDe):
    """
    Electron selection from StdDiElectronFromTracks
    """

    _code = "(MINTREE(ABSID<14,PT) > %(ePT)s) &"\
            "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(eMINIPCHI2)s) &"\
            "(MAXTREE(ABSID<14,TRGHOSTPROB) < %(eGhostProb)s) &"\
            "(MINTREE(ABSID<14,PIDe) > %(ePIDe)s)"  % locals()

    _Filter = FilterDesktop(Code = _code)

    return Selection(name,
                     Algorithm = _Filter,
                     RequiredSelections = [ elecs ]
                     )

#####################################################
def makePionsForeePiPi(name, pions, PionPT, PionMINIPCHI2, PionGhostProb, PionPIDK):
    """
    Pion selection from StdNoPIDsPions
    """
    _code = "(PT > %(PionPT)s) &"\
            "(MIPCHI2DV(PRIMARY) > %(PionMINIPCHI2)s) &"\
            "(TRGHOSTPROB < %(PionGhostProb)s) &"\
            "(PIDK < %(PionPIDK)s)" % locals()
    
    _Filter = FilterDesktop(Code = _code)
    
    return Selection(name,
                     Algorithm = _Filter,
                     RequiredSelections = [ pions ]
                     )

#####################################################
def tisTosSelection(name, sel, dict_TISTOS):
    '''Filters Selection sel to be TOS OR TIS.'''
    from Configurables import TisTosParticleTagger
    tisTosFilter = TisTosParticleTagger(name+'Algo')
    tisTosFilter.TisTosSpecs = dict_TISTOS

    return Selection(name+'Sel',
                     Algorithm=tisTosFilter,
                     RequiredSelections=[sel])

# EOF
