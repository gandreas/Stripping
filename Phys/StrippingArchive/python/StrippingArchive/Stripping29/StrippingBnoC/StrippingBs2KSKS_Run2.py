
"""
Module for construction of Bs->KSKS (including the Bd mass region) stripping Selections and StrippingLines.
Provides functions to build, KS->LL (from StdLooseKsLL), KS->DD (from StdLooseKsDD), Bs->KSLL KSLL (BsLL), Bs->KSLL KSDD (BsLD) and Bs->KSDD KSDD (BsDD) selections.
Provides class Bs2KSKSConf, which constructs the Selections and StrippingLines given a configuration dictionary. 
Exported symbols (use python help!):
    - Bs2KSKSConf
"""

__author__ = ['Moritz Demmer','Timon Schmelzer']
__date__ = '13/09/2016'
__version__ = 'Run2'
__all__ = {'Bs2KSKSConf', 'default_config'}

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

from StandardParticles import StdNoPIDsPions as Pions
from StandardParticles import StdNoPIDsDownPions as DownPions


default_config = {
    'NAME'        : 'Bs2KSKS',
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'Bs2KSKSConf',
    'CONFIG'      : { 'KS0_LL_MassWindow'       : 50.0,
                      'KS0_DD_MassWindow'       : 80.0,
                      'KS0_LL_FDChi2'           : 5,
                      'KS0_DD_FDChi2'           : 5,
                      'KS0_Dira'                : 0.999,
                      ##################################################
                      'B_M_Min'                 : 4000,
                      'B_M_Max'                 : 6500,
                      'B_LL_Doca'               : 1,
                      'B_LD_Doca'               : 4,
                      'B_DD_Doca'               : 4,
                      'B_LL_VtxChi2'            : 20.0,
                      'B_LD_VtxChi2'            : 30.0,
                      'B_DD_VtxChi2'            : 40.0,
                      'B_Dira'                  : 0.999,
                      ##################################################
                      'Trk_Chi2'                : 4,
                      'Trk_Ghost'               : 0.5
                     },
    'STREAMS'     : ['Bhadron'],
    }


class Bs2KSKSConf(LineBuilder) :
    """
    Builder of B->KSKS stripping Selection and StrippingLine.
    Constructs B -> KS KS Selections and StrippingLines from a configuration dictionary.
    Usage:
    >>> config = { .... }
    >>> bs2ksksConf = Bs2KSKSConf('Bs2KSKSTest',config)
    >>> bs2ksksLines = bs2ksksConf.lines
    >>> for line in line :
    >>>  print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:

    selKS2LL               : KS -> Long Long Selection object

    selKS2DD              : KS -> Downstream Downstream Selection object

    selBs2KSLLKSLL            : B -> KS(LL) KS(LL) Selection object
    ll_line                : StrippingLine made out of selBs2KSLLKSLL
    lines                  : List of lines, [ll_line]

    selBs2KSLLKSDD           : B -> KS(LL) KS(DD) Selection object
    ld_line                : StrippingLine made out of selBs2KSLLKSDD
    lines                  : List of lines, [ld_line]

    selBs2KSDDKSDD            : B -> KS(DD) KS(DD) Selection object
    dd_line                : StrippingLine made out of selBs2KSDDKSDD
    lines                  : List of lines, [dd_line]

    Exports as class data member:
    Bs2KSKSConf.__configuration_keys__ : List of required configuration parameters.
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        ll_name = name+'_LL'
        dd_name = name+'_DD'
        ld_name = name+'_LD'

        # self.makeKS2DD( 'KSfor'+dd_name, config )
        self.KS_LL = self.makeKS( 'KSfor' + ll_name, 'LL', config )
        self.KS_DD = self.makeKS( 'KSfor' + dd_name, 'DD', config )


        # self.Bs2KSLLKSLL = makeBs2KSLLKSLL( ll_name, config )
        self.ll_line = StrippingLine(ll_name+'_Run2_Line',
            selection =  self.makeBs2KSKS( ll_name, 'LL', config )
            )

        # self.Bs2KSLLKSDD = makeBs2KSLLKSDD( ld_name, config )
        self.ld_line = StrippingLine(ld_name+'_Run2_Line',
            selection =  self.makeBs2KSKS( ld_name, 'LD', config )
            )

        # self.Bs2KSDDKSDD = makeBs2KSDDKSDD( dd_name, config )
        self.dd_line = StrippingLine(dd_name+'_Run2_Line',
            selection =  self.makeBs2KSKS( dd_name, 'DD', config )
            )

        # Register Lines
        self.registerLine(self.ll_line)
        self.registerLine(self.dd_line)
        self.registerLine(self.ld_line)



    def makeKS( self, name, ks_type, config ) :
        # Define all the cuts
        _mass_cut     = "(ADMASS('KS0')<{}*MeV)".format( config['KS0_' + ks_type + '_MassWindow'] )
        _fd_cut       = "(BPVVDCHI2>{})".format(         config['KS0_' + ks_type + '_FDChi2'] )

        _trkchi2_cut_1 = "(CHILDCUT((TRCHI2DOF<{}),1))".format(    config['Trk_Chi2'] )
        _trkchi2_cut_2 = "(CHILDCUT((TRCHI2DOF<{}),2))".format(    config['Trk_Chi2'] )
        _trkghost_cut_1 = "(CHILDCUT((TRGHOSTPROB<{}),1))".format( config['Trk_Ghost'] )
        _trkghost_cut_2 = "(CHILDCUT((TRGHOSTPROB<{}),2))".format( config['Trk_Ghost'] )

        # Combine the cut
        _all_cuts = '&'.join([_mass_cut, _fd_cut,  # _dira_cut,
                              _trkchi2_cut_1, _trkchi2_cut_2])

        if ks_type == 'LL':
           _all_cuts = '&'.join([_all_cuts, _trkghost_cut_1, _trkghost_cut_2])

        # Get the KS's to filter
        _stdKS = DataOnDemand( Location = 'Phys/StdLooseKs' + ks_type + '/Particles' )

        # Make the filter
        _filterKS = FilterDesktop( Code = _all_cuts )

        return Selection( 'selKS_' + ks_type + '_' + name, Algorithm = _filterKS,
                          RequiredSelections = [_stdKS] )



    def makeBs2KSKS( self, name, ks_type, config ) :
        _mass_min_cut = "(AM>{}*MeV)".format(          config['B_M_Min'] )
        _mass_max_cut = "(AM<{}*MeV)".format(          config['B_M_Max'] )
        _doca_cut     = "(ACUTDOCA({}*mm,''))".format( config['B_' + ks_type + '_Doca'] )

        _combCuts = '&'.join( [_mass_min_cut, _mass_max_cut, _doca_cut] )


        _vtx_cut  = "(VFASPF(VCHI2)<{})".format( config['B_' + ks_type + '_VtxChi2'] )
        _dira_cut = "(BPVDIRA>{})".format(       config['B_Dira'] )
        
        _motherCuts = '&'.join([ _vtx_cut, _dira_cut])
        
        if ks_type == 'LD':
          _motherCuts += " & INTREE((ABSID=='pi+') & (ISLONG)) & INTREE((ABSID=='pi+') & (ISDOWN))"
        
        _B = CombineParticles()
        _B.DecayDescriptors = [ "B_s0 -> KS0 KS0" ]

        _B.CombinationCut = _combCuts
        _B.MotherCut = _motherCuts

        if ks_type == 'LL':
          return Selection(name, Algorithm = _B, RequiredSelections = [ self.KS_LL ])
        if ks_type == 'LD':
          return Selection(name, Algorithm = _B, RequiredSelections = [ self.KS_LL, self.KS_DD ])
        if ks_type == 'DD':
          return Selection(name, Algorithm = _B, RequiredSelections = [ self.KS_DD ])

# StdLooseKsDD:
# DaughtersCuts {'pi+': '(P > 2.*GeV) & (MIPCHI2DV(PRIMARY) > 4.)'}
# CombinationCut  (ADAMASS('KS0') < 80.*MeV) & (ADOCACHI2CUT(25, ''))
# MotherCut (ADMASS('KS0') < 64.*MeV) & (VFASPF(VCHI2) < 25.)

# StdLooseKsLL:
# DaughtersCuts {'pi+': '(P > 2.*GeV) & (MIPCHI2DV(PRIMARY) > 9.)'}
# CombinationCut  (ADAMASS('KS0') < 50.*MeV) & (ADOCACHI2CUT(25, ''))
# MotherCut (ADMASS('KS0') < 35.*MeV) & (VFASPF(VCHI2) < 25.)
