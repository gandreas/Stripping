'''
Module for selecting specific topology
'''

__author__=['Emilie MAURICE']
__date__ = ''
__version__= '$Revision: 1.0 $'


__all__ = (
    'HeavyIonTopologyConf',
    'default_config'
    )

default_config =  {
    'NAME'            : 'HeavyIonTopology',
    'WGs'             : ['IFT'],
    'STREAMS'         : ['IFT'],
    'BUILDERTYPE'     : 'HeavyIonTopologyConf',
    'CONFIG'          : {
        "odin": ["NoBeam","Beam1","Beam2","BeamCrossing"],
        
        "GEC"       : "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 20)",

        'PrescaleLowMult'            :  1.0,
        'PostscaleLowMult'           : 1.0, 

        'PrescaleEE'            :  1.0,
        'PostscaleEE'           : 1.0, 

        'Hlt1FilterLowMult'          : None,
        'Hlt2FilterLowMult'          : None,

        'Hlt1FilterE'          : None, 
        'Hlt2FilterE'          : None,
    }
    }

from Gaudi.Configuration import *
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllLooseMuons, StdAllLoosePions,StdAllNoPIDsPions
from GaudiConfUtils.ConfigurableGenerators import CombineParticles

class HeavyIonTopologyConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)

        from PhysSelPython.Wrappers import Selection, DataOnDemand

        self.name = name 
        self.config = config

        odin = "|".join( ["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,) for odin_type in self.config['odin'] if odin_type in ["NoBeam","Beam1","Beam2","BeamCrossing"]])
        
        _filter = {'Code': config['GEC'], 'Preambulo' : ["from LoKiTracks.decorators import *" ,
                                                "from LoKiCore.functions    import * ",
                                                "from GaudiKernel.SystemOfUnits import *"]}
        
        self.LowActivityLine = StrippingLine( name+'LowActivityLine',
                                              prescale  = self.config['PrescaleLowMult'],
                                              postscale  = self.config['PostscaleLowMult'],
                                              FILTER = _filter,
                                              ODIN      = odin
                                              )
        self.registerLine( self.LowActivityLine )
   
        
        
        odinEE = "(ODIN_BXTYP == LHCb.ODIN.NoBeam)"
        

        self.EmptyEmptyLine = StrippingLine( name+'EmptyEmptyLine',
                                             prescale  = self.config['PrescaleEE'],
                                             postscale  = self.config['PostscaleEE'],
                                             ODIN      = odinEE
                                             )
        self.registerLine( self.EmptyEmptyLine )
