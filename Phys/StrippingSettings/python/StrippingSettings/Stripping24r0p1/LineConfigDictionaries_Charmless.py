################################################################################
##                          S T R I P P I N G  2 4 r 1                        ##
##                                                                            ##
##  Configuration for B2noC WG                                                ##
##  Contact person: Timothy Williams (timothy.williams@cern.ch)               ##
################################################################################

from GaudiKernel.SystemOfUnits import *

#StrippingB2HHBDT.py
B2HHBDT = {
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'B2HHBDTLines',
    'CONFIG'      : { 'PrescaleB2HHBDT' : 1.,
                      'TrChi2'          : 4,
                      'TrGhostProb'     : 3,
                      'PionPT'          : 1000,
                      'SumPT'           : 4500,
                      'DOCACHI2'        : 9,
                      'BIPCHI2'         : 9,
                      'BDIRA'           : 0.99,
                      'BPT'             : 0,
                      'BMassWinLow'     : 4700,
                      'BMassWinHigh'    : 6200,
                      'BMassLow'        : 4800,
                      'BMassHigh'       : 6200,
                      'PionIPCHI2'      : 16,
                      'BFDCHI2'         : 100,
                      'BDTCut'          : -1,
                      'BDTWeightsFile'  : "$TMVAWEIGHTSROOT/data/B2HH_BDT_v1r5.xml"
                    },
    'STREAMS'     : ['Bhadron']
    }

#StrippingB2KShh.py
B2KShh = {
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'B2KShhConf',
    'CONFIG'      : {'Trk_Chi2'                : 4.0,
                     'Trk_GhostProb'           : 0.5,
                     'KS_DD_MassWindow'        : 30.0,
                     'KS_DD_VtxChi2'           : 12.0,
                     'KS_DD_FDChi2'            : 50.0,
                     'KS_DD_Pmin'              : 6000.0,
                     'KS_LD_MassWindow'        : 25.0,
                     'KS_LD_VtxChi2'           : 12.0,
                     'KS_LD_FDChi2'            : 50.0,
                     'KS_LD_Pmin'              : 6000.0,
                     'KS_LL_MassWindow'        : 20.0,
                     'KS_LL_VtxChi2'           : 12.0,
                     'KS_LL_FDChi2'            : 80.0,
                     'KS_LL_Pmin'              : 0.0,
                     'B_Mlow'                  : 1279.0,
                     'B_Mhigh'                 : 921.0,
                     'B_APTmin'                : 1000.0,
                     'BDaug_MedPT_PT'          : 800.0,
                     'BDaug_MaxDOCAChi2'       : 25.0,
                     'BDaug_DD_PTsum'          : 4200.0,
                     'BDaug_LD_PTsum'          : 4200.0,
                     'BDaug_LL_PTsum'          : 3000.0,
                     'B_PTmin'                 : 1500.0,
                     'B_VtxChi2'               : 12.0,
                     'KS_FD_Z'                 : 15.,
                     'B_DD_Dira'               : 0.999,
                     'B_LD_Dira'               : 0.999,
                     'B_LL_Dira'               : 0.999,
                     'B_DD_IPChi2'             : 6.0,
                     'B_LD_IPChi2'             : 7.0,
                     'B_LL_IPChi2'             : 8.0,
                     'B_DD_FDChi2'             : 50.0,
                     'B_LD_FDChi2'             : 50.0,
                     'B_LL_FDChi2'             : 50.0,
                     'BDaug_DD_IPChi2sum'      : 50.0,
                     'BDaug_LD_IPChi2sum'      : 50.0,
                     'BDaug_LL_IPChi2sum'      : 50.0,
                     'GEC_MaxTracks'           : 250,
                     'ConeAngles'              : [ 1.0, 1.5, 1.7, 2.0 ],
                     # Run1 Triggers
                     #'HLT1Dec'                 : 'Hlt1TrackAllL0Decision',
                     #'HLT2Dec'                 : 'Hlt2Topo[234]Body.*Decision',
                     # Run2 Triggers
                     'HLT1Dec'                 : 'Hlt1(Two)?TrackMVADecision|Hlt1(Phi)?IncPhiDecision',
                     'HLT2Dec'                 : 'Hlt2Topo[234]BodyDecision|Hlt2(Phi)?IncPhiDecision',
                     'FlavourTagging'          : False,
                     'MDST'                    : False,
                     'Prescale'                : 1.0,
                     'Prescale_SameSign'       : 1.0,
                     'Postscale'               : 1.0
                     },
    'STREAMS'     : {
                     'BhadronCompleteEvent' : [
                         'StrippingB2KShh_DD_Run2_OS_Line',
                         'StrippingB2KShh_LL_Run2_OS_Line',
                         'StrippingB2KShh_LD_Run2_OS_Line',
                         ] ,
                     'Bhadron' : [
                         'StrippingB2KShh_DD_Run2_SS_Line',
                         'StrippingB2KShh_LL_Run2_SS_Line',
                         'StrippingB2KShh_LD_Run2_SS_Line',
                         ]
                     }
    }

#StrippingXb2phh.py
Xb2phh = {
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'Xb2phhConf',
    'CONFIG'      : {'Trk_MaxChi2Ndof'   : 3.0,
                     'Trk_MaxGhostProb'  : 0.4,
                     'Trk_MinIPChi2'     : 16.0,
                     'Trk_MinP'          : 1500.0,
                     'Trk_MinProbNNp'    : 0.05,
                     'Xb_MinSumPTppi'    : 1500.0,
                     'Xb_MinM'           : 5195.0,
                     'Xb_MaxM'           : 6405.0,
                     'Xb_MinSumPT'       : 3500.0,
                     'Xb_MinPT'          : 1500.0,
                     'Xb_MaxDOCAChi2'    : 20.0,
                     'Xb_MaxVtxChi2'     : 20.0,
                     'Xb_MinFDChi2'      : 50.0,
                     'Xb_MaxIPChi2'      : 16.0,
                     'Xb_MinDira'        : 0.9999,
                     'Prescale'          : 1.0,
                     'Postscale'         : 1.0
                     },
    'STREAMS'     : ['Bhadron']
    }

#StrippingBs2KSKSConf.py
Bs2KSKS = {
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'Bs2KSKSConf',
    'CONFIG'      : { 'KS0_LL_MassWindow'       : 50.0,
                      'KS0_DD_MassWindow'       : 80.0,
                      'KS0_LL_FDChi2'           : 5,
                      'KS0_DD_FDChi2'           : 5,
                      'KS0_Dira'                : 0.999,
                      ##################################################
                      'B_M_Min'                 : 4000,
                      'B_M_Max'                 : 6500,
                      'B_LL_Doca'               : 1,
                      'B_LD_Doca'               : 4,
                      'B_DD_Doca'               : 4,
                      'B_LL_VtxChi2'            : 20.0,
                      'B_LD_VtxChi2'            : 30.0,
                      'B_DD_VtxChi2'            : 40.0,
                      'B_Dira'                  : 0.999,
                      ##################################################
                      'Trk_Chi2'                : 4,
                      'Trk_Ghost'               : 0.5
                     },
    'STREAMS'     : ['Bhadron'],
    }

#StrippingBs2KKhh.py
BsPhiRho = {
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'BsPhiRhoConf',
    'CONFIG'      : {'PRPrescale'     : 1.,
                     'PRResMinPT'     : 900.,
                     'PRResMinP'      : 1.,
                     'PRResMinMass'   : 0.,
                     'PRResMaxMass'   : 4000.,
                     'PRResVtxChiDOF' : 9.,
                     'PRBMinM'        : 4800.,
                     'PRBMaxM'        : 5600.,
                     'PRPhiWindow'    : 25.,
                     'PRBVtxChi2DOF'  : 9.,
                     'PRIPCHI2'       : 20,
                     },
    'STREAMS'     : ['Bhadron']
    }


B2XEta = {
    'WGs'          : ['BnoC'],
    'BUILDERTYPE'  : 'B2XEtaConf',
    'CONFIG'       : {
                  'Trk_Chi2'                : 4.0,
                  'Trk_PT'                  : 300.0,
                  'Trk_GP'                  : 0.5,
                  'pK_PT'                   : 500., #1000.
                  'pK_IPCHI2'               : 20.,
                  'ProbNNCut'               : 0.1,
                  'kstar_daug_PT'           : 500.,
                  'KS_DD_MassWindow'        : 23.0,
                  'KS_DD_VtxChi2'           : 15.0, 
                  'KS_DD_FDChi2'            : 20.0,
                  'KS_DD_PTmin'             : 1200.0,
                  'KS_LL_MassWindow'        : 14.0,
                  'KS_LL_VtxChi2'           : 15.0,  
                  'KS_LL_FDChi2'            : 50.0,
                  'KS_LL_PTmin'             : 1200.0,
                  'L_DD_MassWindow'         : 20.0,
                  'L_DD_VtxChi2'            : 15.0,
                  'L_DD_PTmin'              : 1000.0,
                  'L_LL_MassWindow'         : 15.0,
                  'L_LL_VtxChi2'            : 15.0,
                  'L_LL_PTmin'              : 1000.0,
                  'Kstar_PTmin'             : 1200.0,
                  'Kstar_massWdw'           : 100.0,
                  'Kstar_vtxChi2'           : 9.0,
                  'Kstar_ipChi2'            : 5.0,
                  'eta_PT'                  : 2000,
                  'eta_MassWindow'          : 200.0,
                  'etaforetap_MassWindow'   : 75.0,
                  'eta_vtxChi2'             : 10.,
                  'eta_DOCA'                : 10.0, #20
                  'gamma_PT'                : 500, #photons from eta
                  'eta_prime_MassWindow'    : 150.0,
                  'eta_prime_PT'            : 2000.0,
                  'eta_prime_vtxChi2'       : 10.0,
                  'eta_prime_DOCA'          : 10.0, #15
                  'B_MassWindow'            : 750.0,
                  'B_PTmin'                 : 1500.0,
                  'BDaug_DD_maxDocaChi2'    : 15.0, #20 
                  'BDaug_LL_maxDocaChi2'    : 15.0,  #20
                  'B_VtxChi2'               : 15.0,
                  'B_Dira'                  : 0.9995,
                  'B_IPCHI2'                : 20.0,
                  'B_eta_IPCHI2'            : 6.0, 
                  'Lb_MassWindow'           : 750.0,
                  'Lb_PTmin'                : 1000.0,
                  'LbDaug_DD_maxDocaChi2'   : 15.0, #20
                  'LbDaug_LL_maxDocaChi2'   : 15.0, #20
                  'Lb_VtxChi2'              : 15.0,
                  'Lb_Dira'                 : 0.9995,
                  'Lb_IPCHI2'               : 20.0,
                  'Lb_eta_IPCHI2'           : 6.0,
                  'GEC_MaxTracks'           : 250,
                  'Prescale'                : 1.0,
                  'Postscale'               : 1.0,
                  'etaGG_Prescale'          : 0.0
                  #'TCKs'                : ('0x00470032','0x00790038')
                  #'TCKs'                : ('0x00470032','0x00790038','0x007E003A','0x0097003D','0x407E003A','0x4097003D','0x00990042','0x00AC0046','0x40990042','0x40AC0046')
                  },
    'STREAMS'     : ['Bhadron']
    }
#StrippingB02a1Pi.py
B02a1PiBDT = {
    'BUILDERTYPE'          :  'B02a1PiBDTConf',
    'CONFIG'    : {
        'a1PionCuts'       : "(PROBNNpi > 0.1) & (PT > 200*MeV) & (TRGHOSTPROB<0.4) &  (MIPCHI2DV(PRIMARY) > 2.)",
        'BachelorPionCuts' : "(PROBNNpi > 0.1) & (PT > 1000*MeV) & (TRGHOSTPROB<0.4) &  (MIPCHI2DV(PRIMARY) > 2.)",
        'a1ComCuts'        : "(ADAMASS('a_1(1260)+') < 800 *MeV)",
        'a1Cuts'           : """
                             (MIPCHI2DV(PRIMARY) > 2.)
                             & (VFASPF(VCHI2) < 50.)
                             & (PT > 1.*GeV)
                             """ ,
        'B0ComCuts'        : "(ADAMASS('B0') < 500 *MeV)",
        #'B0ComCuts'        : "(ADAMASS('B0') < 500 *MeV) & ( (ACHILD(PT,1)+ACHILD(PT,2)) > 2.*GeV )",
        'B0MomCuts'        : """
		             (VFASPF(VCHI2) < 50.) 
			     & (BPVDIRA> 0.99) 
			     & (BPVIPCHI2()<30) 
			     & (VFASPF(VMINVDCHI2DV(PRIMARY)) > 10)
			     & (PT > 2.*GeV )
	                     """,
        'B02a1PiMVACut'    :  "-0.0",
        'B02a1PiXmlFile'   :  "$TMVAWEIGHTSROOT/data/B02a1Pi_BDT_v1r1.xml", 
        'Prescale'         : 1.
        },
    'STREAMS'              : ['Bhadron'],
    'WGs'                  : ['BnoC']
    }

#StrippingB2CharmlessInclusive.py
B2CharmlessInclusive = {
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'B2CharmlessInclusive',
    'CONFIG'      : { 'Q2BPrescale'     : 1.0,
                      'Q2BTrkGhostProb' : 0.5,

                      # New cuts
                      'Photon_PT_Min'     : 1300.0,
                      'Photon_Res_PT_Min' : 400.0,
                      'Photon_CL_Min'     : 0.2,

                      'Pi0_B_PT_Min'    : 1300., # Pt cut on a pi0 which is direct daughter of the B
                      'Pi0_Res_PT_Min'  : 500.,  # Pt cut on a pi0 with is grandaughter of the B

                       ###KS cuts                    
                      'KS0_LL_MassWindow'       : 40.0,
                      'KS0_DD_MassWindow'       : 60.0,
                      'KS0_LL_FDChi2'           : 80.,
                      'KS0_DD_FDChi2'           : 50.,
                      'KS0_Dira'                : 0.999,
                      'KS0_Child_Trk_Chi2'       : 4.,
                      ###end KS cuts
                      # End new cuts

                      'Q2BTrkMinIPChi2' : 16.,
                      'Q2BTrkMinPT'     : 400.,
                      'Q2BTrkMinHiPT'   : 1000.,
                      'Q2BResMinPT'     : 600.,
                      'Q2BResMinHiPT'   : 1000.,
                      'Q2BResMaxMass'   : 1100.,
                      'Q2BResVtxChi2DOF': 6.,
                      'Q2BBMinPT'       : 1500.,
                      'Q2BBMinM3pi'     : 4400., #4200.,
                      'Q2BBMinM4pi'     : 3900., #3500.,
                      'Q2BBMaxM3pi'     : 6700.,
                      'Q2BBMaxM4pi'     : 5700.,
                      'Q2BBMaxCorrM3pi' : 7000.,
                      'Q2BBMaxCorrM4pi' : 7000.,
                      'Q2BBVtxChi2DOF'  : 6.            
                      },
    'STREAMS'     : ['Bhadron']     ## This stream puts it into mDST
    }

#StrippingXb2p3h.py
Xb2phhh = {
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'Xb2phhhConf',
    'CONFIG'      : {'Trk_MaxChi2Ndof'   : 4.0,
                     'Trk_MaxGhostProb'  : 0.4,
                     'Trk_MinIPChi2'     : 16.0,
                     'Trk_MinP'          : 1500.0,
                     'Trk_MinProbNNp'    : 0.05,
                     'Xb_MinSumPTppi'    : 1500.0,
                     'Xb_MinSumPTppipi'  : 2500.0,
                     'Xb_MinM_4body'     : 5195.0,
                     'Xb_MaxM_4body'     : 6405.0,
                     'Xb_MinSumPT_4body' : 3500.0,
                     'Xb_MinPT_4body'    : 1500.0,
                     'Xb_MaxDOCAChi2'    : 20.0,
                     'Xb_MaxVtxChi2'     : 20.0,
                     'Xb_MinFDChi2'      : 50.0,
                     'Xb_MaxIPChi2'      : 16.0,
                     'Xb_MinDira'        : 0.9999,
                     'ConeAngles'        : [0.8,1.0,1.3,1.7],
                     'ConeInputs'        : {'Displaced' : ['/Event/Phys/StdNoPIDsPions'], 'Long': ['/Event/Phys/StdAllNoPIDsPions'] },
                     'ConeVariables'     : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                     'Prescale'          : 1.0,
                     'Postscale'         : 1.0
                     },
    'STREAMS'     : ['Bhadron']
    }

#StrippingBu2Rho0Rho0.py
Bu2rho0rhoPlus = {
    "WGs"         : [ "BnoC" ],
    "BUILDERTYPE" : "StrippingBu2rho0rhoPlusConf",
    "STREAMS"     : { "Bhadron" : [ "StrippingBu2rho0rhoPlusMergedLine",
                                    "StrippingBu2rho0rhoPlusResolvedLine",
                                    "StrippingBu2rho0rhoPlusUpMergedLine",
                                    "StrippingBu2rho0rhoPlusUpResolvedLine", ] },
    "CONFIG"      : { "isMC"           : False,  # True = no Hlt filter
                      "refitPVs"       : True,
                      
                      "vetoTrISMUON"   : True,   # ~ISMUON causes errors?
                      "trMinIPChi2"    : 4,
                      "trUpMinIPChi2"  : 8,
                      "trMinProbNNpi"  : 0.0,
                      "trMaxChi2Dof"   : 3.0,
                      "trMaxGhostProb" : 0.5,

                      "longLines"     : { "rhoCombMassMax"        : 1300,   # MeV
                                          "rhoCombMassMin"        : 100,    # MeV
                                          "rhoMothMassMax"        : 1200,   # MeV
                                          "rhoMothMassMin"        : 200,    # MeV
                                          "rho0MinVChi2Dof"       : 14,
                                          
                                          "pi0ResMinCL"           :-1000,
                                          "pi0ResMinP"            : 3500,   # MeV
                                          "pi0ResMinPT"           : 400,    # MeV
                                          "piPRhoPResMinIPChi2"   : 20,
                                          "rho0ResMinIPChi2"      : 20,
                                          "rho0ResMinFDChi2"      : 27,
                                          "rhoPResMinP"           : 7000,   # MeV
                                          "rhoPResMinPT"          : 1000,   # MeV
                                          
                                          "BuResCombMassWindow"   : 650,    # MeV
                                          "BuResMaxTrIPChi2Min"   : 40,
                                          "BuResMaxTrPTMin"       : 1900,   # MeV
                                          "BuResSumTrPTMin"       : 3800,   # MeV
                                          "BuResMinVChi2Dof"      : 8,
                                          "BuResMothMassWindow"   : 600,    # MeV
                                          "BuResMinFDChi2"        : 120,
                                          "BuResMaxIPChi2"        : 30,
                                          "BuResMinDira"          : 0.9998,
                                          "BuResMinPT"            : 1000,   # MeV
                                          
                                          "pi0MgdMinPT"           : 1900,   # MeV
                                          "piPRhoPMgdMinPT"       : 960,    # MeV
                                          "rho0MgdDauMinPT"       : 100,    # MeV
                                          "rho0MgdMinIPChi2"      : 33,
                                          "rho0MgdMinFDChi2"      : 25,
                                          "rho0MgdMaxTrIPChi2Min" : 20,
                                          
                                          "BuMgdCombMassMin"      : 3900,   # MeV
                                          "BuMgdCombMassMax"      : 7150,   # MeV
                                          "BuMgdMaxTrIPChi2Min"   : 20,
                                          "BuMgdMinFDChi2"        : 120,
                                          "BuMgdMaxIPChi2"        : 450,
                                          "BuMgdMinVChi2Dof"      : 8,
                                          "BuMgdMothMassMin"      : 4000,   # MeV
                                          "BuMgdMothMassMax"      : 7000,   # MeV
                                          "BuMgdMinDira"          : 0.9997,
                                          "BuMgdMinPT"            : 4000,   # MeV
                                          },
                      "upstreamLines" : { "rhoCombMassMax"        : 1300,   # MeV
                                          "rhoCombMassMin"        : 100,    # MeV
                                          "rhoMothMassMax"        : 1200,   # MeV
                                          "rhoMothMassMin"        : 200,    # MeV
                                          "rho0MinVChi2Dof"       : 14,
                                          
                                          "pi0ResMinCL"           :-1000,
                                          "pi0ResMinP"            : 1000,   # MeV
                                          "pi0ResMinPT"           : 400,    # MeV
                                          "piPRhoPResMinIPChi2"   : 10,
                                          "rho0ResMinIPChi2"      : 55,
                                          "rho0ResMinFDChi2"      : 55,
                                          "rhoPResMinP"           : 9000,   # MeV
                                          "rhoPResMinPT"          : 1400,   # MeV
                                          
                                          "BuResCombMassWindow"   : 650,    # MeV
                                          "BuResMaxTrIPChi2Min"   : 0,
                                          "BuResMaxTrPTMin"       : 1600,   # MeV
                                          "BuResSumTrPTMin"       : 3500,   # MeV
                                          "BuResMinVChi2Dof"      : 8,
                                          "BuResMothMassWindow"   : 600,    # MeV
                                          "BuResMinFDChi2"        : 55,
                                          "BuResMaxIPChi2"        : 20,
                                          "BuResMinDira"          : 0.9998,
                                          "BuResMinPT"            : 1500,   # MeV
                                          
                                          "pi0MgdMinPT"           : 0,      # MeV
                                          "piPRhoPMgdMinPT"       : 0,      # MeV
                                          "rho0MgdDauMinPT"       : 0,      # MeV
                                          "rho0MgdMinIPChi2"      : 55,
                                          "rho0MgdMinFDChi2"      : 55,
                                          "rho0MgdMaxTrIPChi2Min" : 55,
                                          
                                          "BuMgdCombMassMin"      : 3900,   # MeV
                                          "BuMgdCombMassMax"      : 7150,   # MeV
                                          "BuMgdMaxTrIPChi2Min"   : 55,
                                          "BuMgdMinFDChi2"        : 55,
                                          "BuMgdMaxIPChi2"        : 55,
                                          "BuMgdMinVChi2Dof"      : 8,
                                          "BuMgdMothMassMin"      : 4000,   # MeV
                                          "BuMgdMothMassMax"      : 7000,   # MeV
                                          "BuMgdMinDira"          : 0.999,
                                          "BuMgdMinPT"            : 5000,   # MeV
                                          },
                      
                      "PrescaleBu2rho0rhoPlusResolved"   : 1.0,
                      "PrescaleBu2rho0rhoPlusMerged"     : 1.0,
                      "PrescaleBu2rho0rhoPlusUpResolved" : 1.0,
                      "PrescaleBu2rho0rhoPlusUpMerged"   : 1.0
                      }
    }

B2TwoBaryons = {
    'WGs'               : ['BnoC'],
    'BUILDERTYPE'       : 'B2TwoBaryonLines',
    'STREAMS'           : [ 'BhadronCompleteEvent' ],
    'CONFIG'            :  {
    'PrescaleB2PPbar'   : 1,
    'MinPTB2PPbar'      : 900,
    'MinIPChi2B2PPbar'  : 10,
    #'TrChi2'            : 4,
    'PIDppi'            : -1,
    'PIDpk'             : -2,
    'MaxPTB2PPbar'      : 2100,
    'MaxDaughtPB2PPbar' : 300000,
    'MaxIPChi2B2PPbar'  : 25,
    'CombMassWindow'    : 200,
    'VertexChi2B2PPbar' : 9,
    'BPTB2PPbar'        : 1100,
    'BIPChi2B2PPbar'    : 16,
    'BDIRA'             : 0.9997,
    'MaxGhostProb'      : 0.4,
    'Bs0_APT_Min'                :   2.0*GeV
    , 'Bs0_AM_Max'                 :   700*MeV
    , 'Bs0_ADOCAMAX_Long_Max'        : 5*mm
    , 'Bs0_BPVDIRA_Long_Min'         : 0.9
    , 'Bs0_BPVIPCHI2_Long_Max'       : 25
    , 'Bs0_VtxChi2_NDF_Long_Max'     : 16
    , 'Bs0_BPVVDCHI2_Long_Min'       : 4
    ,
    'Trk_Chi2'                 : 3.0,
    'Lambda_DD_MassWindow'     : 20.0, 
    'Lambda_DD_VtxChi2'        : 12.0,
    'Lambda_DD_FDChi2'         : 50.0,
    'Lambda_DD_FD'             : 300.0,
    'Lambda_DD_Pmin'           : 8000.0,
    'Lambda_LL_MassWindow'     : 15.0,
    'Lambda_LL_VtxChi2'        : 12.0,
    'Lambda_LL_FDChi2'         : 50.0,
    'B_Mlow'                  : 500.0,
    'B_Mhigh'                 : 500.0,
    'B_2bodyMlow'             : 800.0,
    'B_2bodyMhigh'            : 800.0,
    'B_APTmin'                : 1000.0,
    'B_PTmin'                 : 1500.0,
    'BDaug_MedPT_PT'          : 800.0,
    'BDaug_MaxPT_IP'          : 0.05,
    'BDaug_DD_maxDocaChi2'    : 5.0,
    'BDaug_LL_maxDocaChi2'    : 5.0,
    'BDaug_DD_PTsum'          : 4200.0,
    'BDaug_LL_PTsum'          : 3000.0,
    'B_DD_PTMin'             : 500.0,
    'B_LL_PTMin'             : 500.0,
    'B_VtxChi2'               : 12.0,
    'B_DD_Dira'               : 0.995,
    'B_LL_Dira'               : 0.995,
    'B_DD_IPCHI2wrtPV'        : 8.0,
    'B_LL_IPCHI2wrtPV'        : 8.0,
    'B_FDwrtPV'               : 1.0,
    'B_DD_FDChi2'             : 50.0,
    'B_LL_FDChi2'             : 50.0,
    'GEC_MaxTracks'           : 250,
    'Prescale'                : 1.0,
    'Postscale'               : 1.0,
    'MVAResponseLL'           : 0.95,
    'MVAResponseDD'           : 0.97,
    }
    }

#StrippingButo5h.py
Buto5h = {
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'Buto5hBuilder',
    'CONFIG'      : {'MaxTrSIZE'             : 200 ,
                     '_h_PT'                 : 250. ,
                     '_h_IPCHI2'             : 6. ,
                     '_h_TRCHI2DOF'          : 1.7 ,
                     '_h_TRGHP'              : 0.2 ,
                     '_5h_DOCA'              : .14 ,
                     '_5h_DIRA'              : .99999 ,
                     '_5h_FDCHI2'            : 500. ,
                     '_5h_CHI2'              : 12. ,
                     '_5h_PT'                : 1000. ,
                     '_5h_PVIPCHI2sum'       : 400. ,
                     '_5h_Mmax'              : 5679. ,
                     '_5h_Mmin'              : 5079. ,
                     '_probnnpi'             : .15,
                     '_probnnk'              : .20,
                     '_probnnp'              : .05,
                     '5pi_exclLinePrescale'  : 1.0,
                     '5pi_exclLinePostscale' : 1.0,
                     'K4pi_exclLinePrescale' : 1.0,
                     'K4pi_exclLinePostscale': 1.0,
                     'pp3pi_exclLinePrescale' : 1.0,
                     'pp3pi_exclLinePostscale': 1.0,
                     'ppKpipi_exclLinePrescale' : 1.0,
                     'ppKpipi_exclLinePostscale': 1.0
                     },
    'STREAMS'     : ['Bhadron']
    }


Hb2V0V0h = {
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'Hb2V0V0hConf',
    'CONFIG'      : {
                  'Trk_Chi2'                : 4.0,
                  'Trk_GhostProb'           : 0.5,
                  'V0_DD_MassWindow'        : 30.0,
                  'V0_DD_VtxChi2'           : 12.0,
                  'V0_DD_FDChi2'            : 50.0,
                  'V0_LL_MassWindow'        : 20.0,
                  'V0_LL_VtxChi2'           : 12.0,
                  'V0_LL_FDChi2'            : 80.0,
                  'B_Mlow'                  : 1279.0,
                  'B_Mhigh'                 : 921.0,
                  'Hb_Mlow'                 : 400.0,
                  'Hb_Mhigh'                : 400.0,
                  'B_APTmin'                : 1000.0,
                  'B_Dira'                  : 0.999,
                  'B_VtxChi2'               : 12.0,
                  'B_FDChi2'                : 50.0,
                  'B_IPCHI2wrtPV'           : 12.0,
                  'GEC_MaxTracks'           : 250,
                  'HLT1Dec'                 : 'Hlt1(Two)?TrackMVADecision',
                  'HLT2Dec'                 : 'Hlt2Topo[234]BodyDecision',
                  'Prescale'                : 1.0,
                  'Prescale_SameSign'       : 1.0,
                  'Postscale'               : 1.0,
                  'RelatedInfoTools'        : [ { "Type" : "RelInfoConeVariables"
                                                , "ConeAngle" : 1.0
                                                , "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM']
                                                , "Location"  : 'P2ConeVar1'},
                                                { "Type" : "RelInfoConeVariables"
                                                , "ConeAngle" : 1.5
                                                , "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM']
                                                , "Location"  : 'P2ConeVar2'},
                                                { "Type" : "RelInfoConeVariables"
                                                , "ConeAngle" : 1.7
                                                , "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM']
                                                , "Location"  : 'P2ConeVar3'},
                                                {'Type' : 'RelInfoVertexIsolation'
                                                , 'Location': "VtxIsolationInfo"  }
                                                ]
                  },
    'STREAMS'     : [ 'Bhadron' ]
    }

