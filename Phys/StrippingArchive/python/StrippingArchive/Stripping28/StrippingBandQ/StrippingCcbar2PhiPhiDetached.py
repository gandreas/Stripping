'''
Module for selecting Ccbar->PhiPhi, including one line:
DiPhi Detached line, with loose PT, PID cuts, but with IPS cuts on Kaons. 
Inherited from StrippingCcbar2PhiPhi.py; moved To FullDST

'''

__author__=['Jibo He, Liupan An']
__date__ = '11/01/2017'

__all__ = (
    'Ccbar2PhiPhiDetachedConf'
    )

default_config = {
    'NAME'        : 'Ccbar2PhiDetached', 
    'BUILDERTYPE' : 'Ccbar2PhiPhiDetachedConf',
    'CONFIG' : {
        'TRCHI2DOF'        :     5.  ,
        'KaonPIDK'         :     5.  ,
        'KaonPT'           :   650.  , # MeV
        'PhiVtxChi2'       :    16.  ,
        'PhiMassW'         :    12.  , 
        'CombMaxMass'      :  4100.  , # MeV, before Vtx fit
        'CombMinMass'      :  2750.  , # MeV, before Vtx fit
        'MaxMass'          :  4000.  , # MeV, after Vtx fit
        'MinMass'          :  2800.  , # MeV, after Vtx fit
        'Phi_TisTosSpecs'  : { "Hlt1Global%TIS" : 0 },
        'PionCuts'         :  "(PT>0.7*GeV) & (TRCHI2DOF<5) & (MIPCHI2DV(PRIMARY)>36.) & (PIDK<10)" ,
        'KaonCuts'         :  "(PT>0.5*GeV) & (TRCHI2DOF<5) & (MIPCHI2DV(PRIMARY)>25.) & (PIDK>5)",
        'LooseKaonCuts'    :  "(PT>0.5*GeV) & (TRCHI2DOF<5) & (MIPCHI2DV(PRIMARY)>9.)"
        },
    'STREAMS' : [ 'CharmCompleteEvent' ] ,
    'WGs'     : [ 'BandQ' ]
    }

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

from StandardParticles import StdNoPIDsProtons, StdNoPIDsPions, StdNoPIDsDownPions

class Ccbar2PhiPhiDetachedConf(LineBuilder):
    
    __configuration_keys__ = (
        'TRCHI2DOF',
        'KaonPIDK',
        'KaonPT',
        'PhiVtxChi2',
        'PhiMassW', 
        'CombMaxMass',
        'CombMinMass',
        'MaxMass',
        'MinMass',
        'Phi_TisTosSpecs',
        'PionCuts',
        'KaonCuts',
        'LooseKaonCuts'
        )

    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config

        
        """
        Detached 
        """
        self.DetachedPhiForJpsiList = self.createSubSel( OutputList = "DetachedPhiFor" + self.name,
                                                         InputList =  DataOnDemand( Location = 'Phys/StdLooseDetachedPhi2KK/Particles' ), 
                                                         Cuts = "(VFASPF(VCHI2/VDOF)<%(PhiVtxChi2)s)"\
                                                         " & (ADMASS('phi(1020)')<%(PhiMassW)s*MeV )"\
                                                         " & (MAXTREE('K+'==ABSID, TRCHI2DOF) < %(TRCHI2DOF)s )" \
                                                         " & (MINTREE('K+'==ABSID, PIDK)>0)" % self.config)


        self.makeDetachedJpsi2PhiPhi()
        
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )
    
    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "AALL",
                              PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = True)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
    
    
    
    def makeDetachedJpsi2PhiPhi(self):
        DetachedJpsi2PhiPhi = self.createCombinationSel( OutputList = "DetachedJpsi2PhiPhi" + self.name,
                                                         DecayDescriptor = " J/psi(1S) -> phi(1020) phi(1020)", 
                                                         DaughterLists = [ self.DetachedPhiForJpsiList ],                                                          
                                                         PreVertexCuts = "AM>2.65*GeV",
                                                         PostVertexCuts = "(MM>2.7*GeV) & (VFASPF(VCHI2PDOF)<16) & (BPVDLS>10)" %self.config )
        
        DetachedJpsi2PhiPhiLine = StrippingLine( "Ccbar2PhiPhiDetachedLine",
                                                 algos = [ DetachedJpsi2PhiPhi ] )
        
        self.registerLine(DetachedJpsi2PhiPhiLine)

    def filterTisTos(self, name,
                     PhiInput,
                     myTisTosSpecs ) :
        from Configurables import TisTosParticleTagger
        
        myTagger = TisTosParticleTagger(name + "_TisTosTagger")
        myTagger.TisTosSpecs = myTisTosSpecs
        
        # To speed it up, TisTos only with tracking system)
        myTagger.ProjectTracksToCalo = False
        myTagger.CaloClustForCharged = False
        myTagger.CaloClustForNeutral = False
        myTagger.TOSFrac = { 4:0.0, 5:0.0 }
        
        return Selection(name + "_SelTisTos",
                         Algorithm = myTagger,
                         RequiredSelections = [ PhiInput ] )
    
