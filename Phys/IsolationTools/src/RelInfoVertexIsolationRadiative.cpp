#include "Event/Particle.h"
// kernel
#include "GaudiKernel/ToolFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "Kernel/RelatedInfoNamed.h"
#include "Kernel/IDistanceCalculator.h"

// local
#include "RelInfoVertexIsolationRadiative.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RelInfoVertexIsolationRadiative
// Adapted from RelInfoVertexIsolation by Preema Pais
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_TOOL_FACTORY( RelInfoVertexIsolationRadiative )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RelInfoVertexIsolationRadiative::RelInfoVertexIsolationRadiative( const std::string& type,
                                                const std::string& name,
                                                const IInterface* parent)
: GaudiTool ( type, name , parent ),
  m_distCalc   (       NULL         )
{
  declareInterface<IRelatedInfoTool>(this);
  declareProperty("InputParticles", m_inputParticles,
                  "List of containers to check for extra particle vertexing") ;
  declareProperty("MaxChi2", m_chi2 = 9.0,
                  "Maximum chi2 for compatible particles") ;
  declareProperty("Variables", m_variables,
                  "List of variables to store (store all if empty)");
}

//=============================================================================
// Destructor
//=============================================================================
RelInfoVertexIsolationRadiative::~RelInfoVertexIsolationRadiative() {}

//=============================================================================
// Initialize
//=============================================================================
StatusCode RelInfoVertexIsolationRadiative::initialize()
{
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  // If no input particle container is specified, put StdNoPIDsPions by default
  if ( m_inputParticles.empty() )
  {
    m_inputParticles.push_back("/Event/Phys/StdNoPIDsPions") ;
  }

  m_keys.clear();

  if ( m_variables.empty() )
  {

    if ( msgLevel(MSG::DEBUG) ) debug() << "List of variables empty, adding all" << endmsg;
    m_keys.push_back( RelatedInfoNamed::NEWVTXISONUMVTX );
    m_keys.push_back( RelatedInfoNamed::NEWVTXISOTRKRELD0 );
    m_keys.push_back( RelatedInfoNamed::NEWVTXISOTRKDCHI2 );
    m_keys.push_back( RelatedInfoNamed::NEWVTXISODCHI2MASS );

  } else {

    for ( const auto& var : m_variables )
    {
      short int key = RelatedInfoNamed::indexByName( var );
      if (key != RelatedInfoNamed::UNKNOWN) {
        m_keys.push_back( key );
        debug() << "Adding variable " << var << ", key = " << key << endmsg;
      } else {
        warning() << "Unknown variable " << var << ", skipping" << endmsg;
      }
    }

  }

  // get Loki::DistanceCalculator
  m_distCalc  = tool<IDistanceCalculator>("LoKi::DistanceCalculator");

  return sc;
}

//=============================================================================
// Fill Cone Info structure
//=============================================================================
StatusCode RelInfoVertexIsolationRadiative::calculateRelatedInfo( const LHCb::Particle *top,
                                                         const LHCb::Particle *part )
{

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Fill" << endmsg;

  if( !part )
  {
    return Warning( "Found an invalid particle" );
  }

  // Let's not allow the tool to be run on basic particles or photons
  if ( part->isBasicParticle() || isPureNeutralCalo(part) )
  {
    if ( msgLevel(MSG::DEBUG) ) 
      debug() << "Trying to fill isolation for basic particle. Exiting..." << endmsg;
    m_map.clear();
    return StatusCode::SUCCESS;
  }

  // Get the vertex
  const LHCb::Vertex* vtx = part->endVertex() ;
  if ( msgLevel(MSG::DEBUG) )
  {
    debug() << "vertex for P, ID " << part->particleID().pid()
            << " = " << vtx << " at " << vtx->position() << endmsg;
  }

  if ( !vtx )
    return Error("Can't retrieve the vertex!") ;
  
  const double originalVtxChi2 = vtx->chi2() ;
  
  if ( msgLevel(MSG::DEBUG) )
   {
     debug() << "Vertex chi2 " <<  originalVtxChi2 << endmsg;
   }

  // -- Clear the vector with the particles in the specific decay
  m_signalFinalState.clear();
  m_particlesToVertex.clear();
  // -- Add the mother (prefix of the decay chain) to the vector
  if ( msgLevel(MSG::DEBUG) )
    debug() << "Filling particle with ID " << top->particleID().pid() << endmsg;
  // -- Save all basic particles that belong to the mother vertex in the vector m_signalFinalState
  findSignalFinalState( part );

  // -- Get vector of particles excluding the signal
  LHCb::Particle::ConstVector partsToCheck ;
  for ( const auto& location : m_inputParticles )
  {
    // Get the particles
    LHCb::Particle::Range particles = getIfExists<LHCb::Particle::Range>(location+"/Particles") ;
    if (msgLevel(MSG::DEBUG))
      debug() << "Got " << particles.size() << " particles from " << location << endmsg ;
    if ( particles.empty() ) continue ;

    // Loop over the particles and take the ones we can use for vertexing
    for ( const auto* particle : particles )
    {
      // Ignore if no proto
      if ( !particle->proto() )        continue ;
      // Also ignore if neutral
      if ( isPureNeutralCalo(particle) ) continue ;
      // Check that the proto of the particle does not match the signal
      bool isSignal = false ;
      for ( auto iSignal = m_signalFinalState.begin(), iSignalEnd = m_signalFinalState.end();
            iSignal != iSignalEnd;
            ++iSignal )
      {
        const LHCb::ProtoParticle* pSignal = (*iSignal)->proto() ;
        if ( pSignal && (pSignal == particle->proto() ) )
        {
          isSignal = true ;
          break ;
        }
      }
      if ( isSignal ) continue ;

      // Check for duplicated particles
      bool isAlreadyIn = false ;
      for ( const auto* savedParticle : partsToCheck )
      {
        const LHCb::ProtoParticle* pSaved = savedParticle->proto() ;
        if ( pSaved && ( pSaved == particle->proto() ) )
        {
          isAlreadyIn = true ;
          break ;
        }
      }
      if ( !isAlreadyIn ) partsToCheck.push_back( particle ) ;
    }
  }
  if ( msgLevel(MSG::DEBUG) )
    debug() << "Number of particles to check excluding signal, particles with same proto and gammas = "
            << partsToCheck.size() << endmsg;
  
  // Now compute track d0 chi2 relative to the vertex, find minimum value
    
  if ( msgLevel(MSG::VERBOSE) ) verbose() << "Filling isolation variables" << endmsg;
  
  IsolationResult isolationFirstTrack = getIsolation(vtx, partsToCheck) ;
  double smallestMass  = 0.0 ;
  
  if ( isolationFirstTrack.bestParticle )
    {
      const Gaudi::LorentzVector cand = part->momentum() + isolationFirstTrack.bestParticle->momentum();
      smallestMass = cand.mass();
    }
  
  // Save values
  m_nPartChi2Win      = isolationFirstTrack.nCompatibleChi2 ;
  m_smallestRelD0     = isolationFirstTrack.smallestRelD0;
  m_smallestDChi2     = isolationFirstTrack.smallestDChi2;
  m_smallestDChi2Mass = smallestMass;

  m_map.clear();

  for ( const auto key : m_keys )
  {

    float value = 0;
    switch (key) {
    case RelatedInfoNamed::NEWVTXISONUMVTX         : value = (float) m_nPartChi2Win; break;
    case RelatedInfoNamed::NEWVTXISOTRKRELD0       : value = m_smallestRelD0; break;
    case RelatedInfoNamed::NEWVTXISOTRKDCHI2       : value = m_smallestDChi2; break;    
    case RelatedInfoNamed::NEWVTXISODCHI2MASS      : value = m_smallestDChi2Mass; break;    
    default: value = 0.; break;
    }
    debug() << "  Inserting key = " << key << ", value = " << value << " into map" << endmsg;
    m_map.insert( std::make_pair( key, value) );
  }

  // We're done!
  return StatusCode::SUCCESS ;
}

//=============================================================================
// Get (recursively) the particles to vertex in the decay chain
//=============================================================================
//bool  RelInfoVertexIsolationRadiative::getIsolation( const LHCb::Vertex*,
//                                     LHCb::Particle::ConstVector &extraParticles )
RelInfoVertexIsolationRadiative::IsolationResult
RelInfoVertexIsolationRadiative::getIsolation(const LHCb::Vertex* vertex,
                                      LHCb::Particle::ConstVector &extraParticles )
{
  int             nCompatibleChi2     = 0 ;
  double          smallestRelD0       = -1 ;
  double          smallestDChi2       = -1 ;
  LHCb::Particle *bestParticle        = NULL ;

  IsolationResult res ;
  
  for ( const auto* extraPart : extraParticles )
    {
      // use DistanceCalculator to find the track with closest approach, and its' impact parameter chi2

      double trkVtxD0  = -999.;
      double trkD0Chi2 = -999.;
      
      StatusCode sc2;
      sc2 = m_distCalc->distance(extraPart, vertex, trkVtxD0, trkD0Chi2);
      if (sc2 != StatusCode::SUCCESS) {
	trkVtxD0   = -999.;
	trkD0Chi2  = -999.;
      }

      if ( msgLevel(MSG::VERBOSE) )
	verbose() << "Track with relative impact parameter " << trkVtxD0 
		  << " has a chi2-value = " << trkD0Chi2 
		  << " with respect to the vertex" << endmsg ;
      
      // An impact parameter chi2-value of -999. means that the particle was not compatible with the vertex
      // Fix this to print warning in case sc2 is a success but still get a negative D0Chi2 value
      if ( trkD0Chi2 <= 0 ) continue;
      
      // Get values of deltas, n particles, etc.
      if ( (m_chi2 > 0.0) && ((vertex->chi2() + trkD0Chi2) < m_chi2) ) nCompatibleChi2++ ;
      if ( msgLevel(MSG::DEBUG) ){
	if( smallestDChi2 > 0. && ( smallestDChi2 > trkD0Chi2 ) && ( trkVtxD0 > smallestRelD0)){
	  debug() << "Particle with smallest impact parameter chi2-value does not have the smallest impact parameter" <<  endmsg ;
	  debug() << "Particle impact parameter, associated chi2-value (" << trkVtxD0 << ", " << trkD0Chi2 
		  << ") compared to current minimum values ("  << smallestRelD0 << ", " << smallestDChi2 << ")" << endmsg ;
	}
      }
      if ( smallestDChi2 < 0. || ( smallestDChi2 > trkD0Chi2 )) {
	smallestDChi2 = trkD0Chi2;
	smallestRelD0 = trkVtxD0;
	bestParticle = (LHCb::Particle*) (extraPart) ;
      }
    }
  
  res.nCompatibleChi2    = nCompatibleChi2 ;
  res.smallestRelD0      = smallestRelD0 ;
  res.smallestDChi2      = smallestDChi2 ;
  res.bestParticle       = bestParticle ;
  
  //  }
  
  return res ;
}

//=============================================================================
// Get (recursively) the particles to vertex in the decay chain
//=============================================================================
void RelInfoVertexIsolationRadiative::findSignalFinalState( const LHCb::Particle *particle )
{
  // -- Fill all the daugthers in m_signalFinalState
  for ( const auto* dau : particle->daughtersVector() )
  {
    // Recurse (will not do anything for basic particles)
    findSignalFinalState( dau );
    // -- If the particle has proto and is not a photon, add to the list of final states
    if ( dau->proto() && !isPureNeutralCalo(dau) ) m_signalFinalState.push_back( dau );
  }
}

LHCb::RelatedInfoMap* RelInfoVertexIsolationRadiative::getInfo(void)
{
  return &m_map;
}

