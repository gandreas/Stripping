#!/usr/bin/env python

gangadir = '/afs/cern.ch/user/r/rvazquez/work/Stripping28_Patches/'

jobs = {
  "B2CC"      : "B2CC",
  "B2OC"      : "B2OC",
  "BandQ"     : "BandQ",
  "Charm"     : "Charm",
  "BnoC"      : "BnoC",
  "QEE"       : "QEE",
  "RD"        : "RD",
  "SL"        : "SL",
  "Calib"     : "Calib"
}

import os
from copy import copy

for (wg, job) in jobs.iteritems() :
  d = {}

  path = gangadir + str(job)
  files = os.listdir(path)
  curr_stream = None
  nevents = 0
  nEventsLines = 0
  nTotalEvents = 0
  nEventsStream = 0
  for filename in files :
    reachedLastEvent = False
    foundStrippingReport = False

    fullname = path + '/' + filename
    if not os.path.isfile(fullname) : continue
    f = file(fullname)
    if "log" in filename:
      for l in f :
        if l.find('Xeon') > 0 :
            speed = float(l.split()[8])
        if l.find('Application Manager Stopped successfully') > 0 :
            reachedLastEvent = True
            continue
        if reachedLastEvent :
            if l.find('StrippingReport') == 0:
                nevents += int(l.split()[6])
            if l.find('StrippingGlobal') > 0 :
                foundStrippingReport = True
                ls = l.split('|')
                nTotalEvents = int(ls[3])
            elif foundStrippingReport and l.find('StrippingSequenceStream') > 0 :
                ls = l.split('|')
                name = ls[1].strip().rstrip('_')[24:]
                curr_stream = name
                nev = int(ls[3])
                nEventsStream += nev
            elif foundStrippingReport and l.find('Stripping') > 0 :
                ls = l.split('|')
                name = ls[1].strip()[1:]
                nev = int(ls[3])
                nEventsLines += nev
            elif foundStrippingReport :
                break
    f.close()
  #print "In %s, overlap in Streams = %s, overlap in Lines = %s" %(wg, nEventsStream/float(nTotalEvents), nEventsLines/float(nTotalEvents))
  print "In %s, overlap in Lines = %s" %(wg, nEventsLines/float(nTotalEvents))


