#ifndef STRIPPINGREPORT_H
#define STRIPPINGREPORT_H 1

// Include files
// from Gaudi
//#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IChronoStatSvc.h"
#include "GaudiKernel/IIncidentListener.h"

/** @class StrippingReport StrippingReport.h
 *
 *
 *  @author Anton Poluektov
 *  @date   2010-03-15
 */

class IAlgManager;
class IIncidentSvc;

/// Report structure
class ReportStat {

  public:

    std::string name;      /// Selection name
    int decisions;         /// Number of selected events
    int candidates;        /// Number of candidates
    double avgtime;        /// Average time per event
    int errors;            /// Number of errors produced by the line
    int incidents;         /// Number of incidents produced by the line (usually, too many candidates per event)
    int slow_events;       /// Number of slow events
    std::vector<int> corr; /// Number of events also selected by another selection
};


class StrippingReport : public GaudiTupleAlg,
                        virtual public IIncidentListener {
public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

  void handle ( const Incident& ) override;

private:

  void report(bool onlyPositive);
  void reportLatex(bool onlyPositive);
  void correlationReport(bool linesNotStreams);

  std::vector < ReportStat > m_stat;

  IAlgManager* m_algMgr;                 ///< Pointer to algorithm manager
  IChronoStatSvc* m_chronoSvc;           ///< Chrono service
  IIncidentSvc* m_incSvc;                ///< Incident service

  long m_event;                   ///< Event count
  long m_goodEvent;               ///< Good event count (after pre-filter)


  Gaudi::Property<std::string> m_hdrLocation 
    {this, "HDRLocation", "", 
    "Location of the stripping HltDecReport"};

  Gaudi::Property<std::vector <std::string>> m_selections 
    {this, "Selections", {},
    "List of selections to report"};

  Gaudi::Property<bool> m_onlyPositive 
    {this, "OnlyPositive", true, 
    "If true, show only selections with positive decision"};

  Gaudi::Property<bool> m_everyEvent 
    {this, "EveryEvent", false, 
    "If true, show selection statistics for every event"};

  Gaudi::Property<int> m_reportFreq 
    {this, "ReportFrequency", 100, 
    "Frequency of reports during run processing"};

  Gaudi::Property<bool> m_printNonResponding 
    {this, "PrintNonResponding", false, 
    "If true, show non-responding (with zero events selected) lines"};

  Gaudi::Property<bool> m_printHot 
    {this, "PrintHot", false, 
    "If true, show hot (with rate above threshold) lines"};

  Gaudi::Property<double> m_hotThreshold 
    {this, "HotThreshold", 0.0005, 
    "Rate threshold for hot lines"};

  Gaudi::Property<bool> m_normalizeByGoodEvents 
    {this, "NormalizeByGoodEvents", true, 
    "If true, use good event number (after pre-filter) to calculate rates"};

  Gaudi::Property<bool> m_latex 
    {this, "Latex", false, 
    "false for TWiki, true for LaTeX style"};

  Gaudi::Property<bool> m_correlation 
    {this, "Correlation", false, 
    "If true, includes counters for events selected by two lines"};

  Gaudi::Property<float> m_correlationGreenThreshold 
    {this, "CorrelationGreenThreshold", 0.02, 
    "Threshold on correlation to set a green warning"};

  Gaudi::Property<float> m_correlationYellowThreshold 
    {this, "CorrelationYellowThreshold", 0.05, 
    "Threshold on correlation to set a yellow warning"};

  Gaudi::Property<float> m_correlationRedThreshold 
    {this, "CorrelationRedThreshold", 0.3, 
    "Threshold on correlation to set a red warning"};

  Gaudi::Property<std::string> m_strippingGlobalName 
    {this, "StrippingGlobalName", "StrippingGlobal", 
    "Name of the StrippingGlobal stream, to avoid meaningless correlation."};

  Gaudi::Property<std::string> m_outputfile 
    {this, "OutputFile", "", 
    "Name of the file to save the output."};

  Gaudi::Property<float> m_hlt2rate 
    {this, "HLT2Rate", 0., 
    "HLT2 output rate [Hz] so retentions can be calculated in Hz."};

};

#endif // STRIPPINGREPORT_H
