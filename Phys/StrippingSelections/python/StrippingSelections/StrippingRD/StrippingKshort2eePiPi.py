
__author__ = ['Carla Marin']
__date__ = '23/11/2016'
__version__ = '$1.1 $'

'''
    stripping code for Kshort --> eePiPi
'''

default_config =  {
    'NAME'        : 'Kshort2eePiPi',
    'WGs'         : ['RD'],
    'BUILDERTYPE' : 'Kshort2eePiPiConf',
    'CONFIG'      : {   'Kshort2eePiPi_eeFromTracksLinePrescale'        : 1 ,
                        'Kshort2eePiPi_eeFromTracksLinePostscale'       : 1 ,
                        'Kshort2eePiPi_eeFromTracks_TOSLinePrescale'    : 1 ,
                        'Kshort2eePiPi_eeFromTracks_TOSLinePostscale'   : 1 ,
                        
                        'ePT'           : 100.          , #MeV
                        'eMINIPCHI2'    : 16            , #adimensional
                        'ePIDe'         : -4            , #adimensional
                        'eGhostProb'    : 0.5           , #adimensional
                        'PionPT'        : 100           , #MeV
                        'PionMINIPCHI2' : 16            , #adimensional
                        'PionPIDK'      : 5             , #adimensional
                        'PionGhostProb' : 0.5           , #adimensional    
                        'KsMAXDOCA'     : 1.            , #mm
                        'KsLifetime'    : 0.01*89.53    , #0.01*10^-12s
                        'KsIP'          : 1             , #mm
                        'MaxKsMass'     : 800.          , #MeV, comb mass high limit
                        'KsVtxChi2'     : 50            ,

                        # TOS line
                        'ePT_soft'             : 50.           , #MeV # 80 at Hlt2
                        'eMINIPCHI2_soft'      : 0.            , #adimensional # no cut at Hlt2
                        'ePIDe_soft'           : -100          , #adimensional # -2 at Hlt2
                        'eGhostProb_soft'      : 0.5           , #adimensional
                        'PionPT_soft'          : 400.          , #MeV # 500 at Hlt2
                        'PionMINIPCHI2_soft'   : 4.            , #adimensional # 9 at Hlt2
                        'PionPIDK_soft'        : 100.          , #adimensional # no cut at Hlt2
                        'PionGhostProb_soft'   : 0.5           , #adimensional
                        'KsMAXDOCA_soft'       : 1.            , #mm # 0.5 at Hlt2
                        'KsLifetime_soft'      : 0.            , #s # no cut at Hlt2
                        'KsIP_soft'            : 1.            , #mm # 0.5 at Hlt2
                        'MaxKsMass_soft'       : 650.          , #MeV # 550 at Hlt2
                        'KsVtxChi2_soft'       : 25.           , # 16 at Hlt2
                        
                        'TISTOSDict'     : {'Hlt2RareStrangeKsPiPiEETOSDecision%TOS': 0,
                                            },

                    },
    'STREAMS'     : ['Leptonic']
    }

__all__ = ('Kshort2eePiPiConf',
           'default_config'
           )

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays as Combine3Particles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllNoPIDsPions
from StandardParticles import StdDiElectronFromTracks

class Kshort2eePiPiConf(LineBuilder) :
    """
    Builder for Kshort --> pi,pi,e,e
    """
    
    __configuration_keys__ = default_config['CONFIG'].keys()
                              
    def __init__(self, name, config):

        self.name = name
        LineBuilder.__init__(self, name, config)
        
        # 1 : Make e's for Kshort2eePiPi
        selElecsFromTracksForeePiPi = makeElecsFromTracksForeePiPi("ElecsFromTracksFor"+self.name,
                                                                   StdDiElectronFromTracks,
                                                                   config["ePT"],
                                                                   config["eMINIPCHI2"],
                                                                   config["eGhostProb"],
                                                                   config["ePIDe"]
                                                                   )

        selElecsFromTracksForeePiPiSoft = makeElecsFromTracksForeePiPi("ElecsFromTracksFor"+self.name+"_soft",
                                                                       StdDiElectronFromTracks,
                                                                       config["ePT_soft"],
                                                                       config["eMINIPCHI2_soft"],
                                                                       config["eGhostProb_soft"],
                                                                       config["ePIDe_soft"]
                                                                      )

        # 2 : Make Pions for Kshort2eePiPi
        selPionsForeePiPi = makePionsForeePiPi("PionsFor"+self.name,
                                               StdAllNoPIDsPions,
                                               config["PionPT"],
                                               config["PionMINIPCHI2"],
                                               config["PionGhostProb"],
                                               config["PionPIDK"]
                                               )

        selPionsForeePiPiSoft = makePionsForeePiPi("PionsFor"+self.name+"_soft",
                                                   StdAllNoPIDsPions,
                                                   config["PionPT_soft"],
                                                   config["PionMINIPCHI2_soft"],
                                                   config["PionGhostProb_soft"],
                                                   config["PionPIDK_soft"]
                                               )
        
        # 3 : Combine
        selKshort2eePiPiFromTracks = self._makeKshort2eePiPi(self.name+"_eeFromTracks",
                                                             "KS0 -> pi+ pi- J/psi(1S)",
                                                             selPionsForeePiPi,
                                                             selElecsFromTracksForeePiPi,
                                                             config["MaxKsMass"],
                                                             config["KsMAXDOCA"],
                                                             config["KsIP"],
                                                             config["KsLifetime"],
                                                             config["KsVtxChi2"]
                                                             )
 
        selKshort2eePiPiFromTracks_forTOS = self._makeKshort2eePiPi(self.name+"_eeFromTracks"+"_forTOS",
                                                                    "KS0 -> pi+ pi- J/psi(1S)",
                                                                    selPionsForeePiPiSoft,
                                                                    selElecsFromTracksForeePiPiSoft,
                                                                    config["MaxKsMass_soft"],
                                                                    config["KsMAXDOCA_soft"],
                                                                    config["KsIP_soft"],
                                                                    config["KsLifetime_soft"],
                                                                    config["KsVtxChi2_soft"]
                                                                    )

        # 3b : trigger filter
        selKshort2eePiPiFromTracks_TOS = tisTosSelection(self.name+"_eeFromTracks"+"_TOS",
                                                         selKshort2eePiPiFromTracks_forTOS,
                                                         config['TISTOSDict'])
        # 4 : Declare Lines
        self.eePiPiFromTracksLine = StrippingLine(self.name+"_eeFromTracks"+"Line",
                                                  prescale  = config['Kshort2eePiPi_eeFromTracksLinePrescale'],
                                                  postscale = config['Kshort2eePiPi_eeFromTracksLinePostscale'],
                                                  selection = selKshort2eePiPiFromTracks,
                                                  MDSTFlag  = False
                                                  )

        self.eePiPiFromTracksTOSLine = StrippingLine(self.name+"_eeFromTracks"+"_TOSLine",
                                                     prescale  = config['Kshort2eePiPi_eeFromTracks_TOSLinePrescale'],
                                                     postscale = config['Kshort2eePiPi_eeFromTracks_TOSLinePostscale'],
                                                     selection = selKshort2eePiPiFromTracks_TOS,
                                                     MDSTFlag  = False
                                                     )

        # 5 : register Line
        self.registerLine( self.eePiPiFromTracksLine )
        self.registerLine( self.eePiPiFromTracksTOSLine )
    
#####################################################
    def _makeKshort2eePiPi(self, name, decay, pionSel, elecSel, MaxKsMass, KsMAXDOCA, KsIP, KsLifetime, KsVtxChi2):
        """
        Handy interface for Kshort2eePiPi
        """
        return makeKshort2eePiPi(name,
                                 decay,
                                 pionSel,
                                 elecSel,
                                 MaxKsMass,
                                 KsMAXDOCA,
                                 KsIP,
                                 KsLifetime,
                                 KsVtxChi2
                                 )

#####################################################
def makeKshort2eePiPi(name, decay, pionSel, elecSel, MaxKsMass, KsMAXDOCA, KsIP, KsLifetime, KsVtxChi2):
    """
    Makes the KS0 -> pi+ pi- (J/psi(1S) -> e+ e-)
    """

    _comb12cut =    "(AMAXDOCA('') < %(KsMAXDOCA)s *mm)" % locals()
    
    _combcut =      "(AM < %(MaxKsMass)s *MeV) & "\
                    "(AMAXDOCA('') < %(KsMAXDOCA)s *mm)" % locals()
    
    
    _mothercut =    "(M < %(MaxKsMass)s *MeV) &"\
                    "(MIPDV(PRIMARY) < %(KsIP)s *mm) & "\
                    "((BPVVDSIGN*M/P) > %(KsLifetime)s*2.9979e-01) & "\
                    "(VFASPF(VCHI2/VDOF) < %(KsVtxChi2)s) " % locals()

    _Combine = Combine3Particles(DecayDescriptor=decay,
                                 Combination12Cut=_comb12cut,
                                 CombinationCut=_combcut,
                                 MotherCut=_mothercut
                                 )

    return Selection(name,
                     Algorithm=_Combine,
                     RequiredSelections=[elecSel,pionSel]
                     )

#####################################################
def makeElecsFromTracksForeePiPi(name, elecs, ePT, eMINIPCHI2, eGhostProb, ePIDe):
    """
    Electron selection from StdDiElectronFromTracks
    """

    _code = "(MINTREE(ABSID<14,PT) > %(ePT)s) &"\
            "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(eMINIPCHI2)s) &"\
            "(MAXTREE(ABSID<14,TRGHOSTPROB) < %(eGhostProb)s) &"\
            "(MINTREE(ABSID<14,PIDe) > %(ePIDe)s)"  % locals()

    _Filter = FilterDesktop(Code = _code)

    return Selection(name,
                     Algorithm = _Filter,
                     RequiredSelections = [ elecs ]
                     )

#####################################################
def makePionsForeePiPi(name, pions, PionPT, PionMINIPCHI2, PionGhostProb, PionPIDK):
    """
    Pion selection from StdNoPIDsPions
    """
    _code = "(PT > %(PionPT)s) &"\
            "(MIPCHI2DV(PRIMARY) > %(PionMINIPCHI2)s) &"\
            "(TRGHOSTPROB < %(PionGhostProb)s) &"\
            "(PIDK < %(PionPIDK)s)" % locals()
    
    _Filter = FilterDesktop(Code = _code)
    
    return Selection(name,
                     Algorithm = _Filter,
                     RequiredSelections = [ pions ]
                     )

#####################################################
def tisTosSelection(name, sel, dict_TISTOS):
    '''Filters Selection sel to be TOS OR TIS.'''
    from Configurables import TisTosParticleTagger
    tisTosFilter = TisTosParticleTagger(name+'Algo')
    tisTosFilter.TisTosSpecs = dict_TISTOS

    return Selection(name+'Sel',
                     Algorithm=tisTosFilter,
                     RequiredSelections=[sel])

# EOF