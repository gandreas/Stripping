#!/bin/bash

lb-dev DaVinci/v42r3  #Version of DaVinci will probably change for each campaign, usually announced or detailed on stripping twiki.
cd DaVinciDev_v42r3
git lb-use Stripping
git lb-checkout Stripping/master Phys/StrippingSelections
git lb-checkout Stripping/master Phys/StrippingSettings
getpack TMVAWeights HEAD  #Only required if there are lines with TMVA Weights files not yet in a released version of TMVAWeights
make configure
make
make install