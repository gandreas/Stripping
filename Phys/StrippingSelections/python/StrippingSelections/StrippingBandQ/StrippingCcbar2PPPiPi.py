'''
Module for selecting charmonium -> p pbar pi pi 
'''

__author__=['Jibo He']
__date__ = '12/11/2015'
__version__= '$Revision: 1.0 $'


__all__ = (
    'Ccbar2PPPiPiConf',
    'default_config'
    )

default_config = {
    'NAME'              :  'Ccbar2PPPiPi',
    'BUILDERTYPE'       :  'Ccbar2PPPiPiConf',
    'CONFIG'    : {
        'ProtonCuts'    : "(PROBNNp  > 0.05) & (PT > 400*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) > 1.)",
        'PionCuts'      : "(PROBNNpi > 0.2 ) & (PT > 150*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) > 2.)",
        'EtacComAMCuts' : "AALL",
        'EtacComN4Cuts' : """
                          (AM > 2.7 *GeV)
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.5 *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>30)
                          """,
        'EtacMomN4Cuts' : "(VFASPF(VCHI2/VDOF) < 9.) & (MM>2.8 *GeV) & (BPVDLS>5)",
        'MvaCut'        : "0.19",
        'XmlFile'       : "$TMVAWEIGHTSROOT/data/Ccbar2PPPiPi_BDT_v1r0.xml",
        'Prescale'      : 1.
        },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['BandQ'],
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays

class Ccbar2PPPiPiConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config


        self.SelProtons = self.createSubSel( OutputList = self.name + "SelProtons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLooseANNProtons/Particles' ), 
                                           Cuts = config['ProtonCuts']
                                           )

        self.SelPions = self.createSubSel( OutputList = self.name + "SelPions",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLooseANNPions/Particles' ), 
                                           Cuts = config['PionCuts']
                                           )

        """
        J/psi -> p pbar Pi Pi
        """
        self.SelCcbar2PPPiPi = self.createN4BodySel( OutputList = self.name + "SelCcbar2PPPiPi",
                                                     DaughterLists = [ self.SelProtons, self.SelPions ],
                                                     DecayDescriptor = "J/psi(1S) -> p+ p~- pi+ pi-",
                                                     ComAMCuts      = config['EtacComAMCuts'],
                                                     PreVertexCuts  = config['EtacComN4Cuts'], 
                                                     PostVertexCuts = config['EtacMomN4Cuts']
                                                     )
        

        """
        BDT based 
        """
        self.Ccbar2PPPiPiVars = {
            "sqrt(ProtonP_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 1 ))",
            "sqrt(ProtonM_IPCHI2_OWNPV)"      : "sqrt(CHILD(MIPCHI2DV(), 2 ))",
            "sqrt(PionP_IPCHI2_OWNPV)"        : "sqrt(CHILD(MIPCHI2DV(), 3 ))",
            "sqrt(PionM_IPCHI2_OWNPV)"        : "sqrt(CHILD(MIPCHI2DV(), 4 ))",
            "ProtonP_ProbNNpi"                : "(CHILD(PROBNNpi, 1))",
            "ProtonM_ProbNNpi"                : "(CHILD(PROBNNpi, 2))",
            "ProtonP_ProbNNk"                 : "(CHILD(PROBNNk,  1))",
            "ProtonM_ProbNNk"                 : "(CHILD(PROBNNk,  2))",
            "ProtonP_ProbNNp"                 : "(CHILD(PROBNNp,  1))",
            "ProtonM_ProbNNp"                 : "(CHILD(PROBNNp,  2))",
            "PionP_ProbNNpi"                  : "(CHILD(PROBNNpi, 3))",
            "PionM_ProbNNpi"                  : "(CHILD(PROBNNpi, 4))",
            "PionP_ProbNNk"                   : "(CHILD(PROBNNk,  3))",
            "PionM_ProbNNk"                   : "(CHILD(PROBNNk,  4))",
            "sqrt(ProtonP_IPCHI2_OWNPV+ProtonM_IPCHI2_OWNPV+PionP_IPCHI2_OWNPV+PionM_IPCHI2_OWNPV)"  : "sqrt( CHILD(MIPCHI2DV(),1) + CHILD(MIPCHI2DV(),2) + CHILD(MIPCHI2DV(),3) + CHILD(MIPCHI2DV(),4))",
            "sqrt(Jpsi_IPCHI2_OWNPV)"         : "sqrt(BPVIPCHI2())",
            "log(ProtonP_PT)"                 : "log(CHILD(PT, 1))",
            "log(ProtonM_PT)"                 : "log(CHILD(PT, 2))",
            "log(PionP_PT)"                   : "log(CHILD(PT, 3))",
            "log(PionM_PT)"                   : "log(CHILD(PT, 4))",
            "log(Jpsi_PT)"                    : "log(PT)",
            "Jpsi_ENDVERTEX_CHI2"             : "VFASPF(VCHI2)" 
        }

        self.MvaCcbar2PPPiPi = self.applyMVA( self.name + "MvaCcbar2PPPiPi",
                                           SelB        = self.SelCcbar2PPPiPi,
                                           MVAVars     = self.Ccbar2PPPiPiVars,
                                           MVACutValue = config['MvaCut'], 
                                           MVAxmlFile  = config['XmlFile']
                                           )
        
        self.Ccbar2PPPiPiLine = StrippingLine( self.name + 'Line',                                                
                                               prescale  = config['Prescale'],                                               
                                               algos     = [ self.MvaCcbar2PPPiPi ],
                                               MDSTFlag  = False
                                               )
        self.registerLine( self.Ccbar2PPPiPiLine )
        

        
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )
    
    def createN4BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N4BodyDecays ( DecayDescriptor = DecayDescriptor,      
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts + "&" + "( ACHI2DOCA(1,2)<20 )",
                                           Combination123Cut = ComAMCuts + "&" + "( ACHI2DOCA(1,3)<20 ) & ( ACHI2DOCA(2,3)<20 )",
                                           CombinationCut = "( ACHI2DOCA(1,4)<20 ) & ( ACHI2DOCA(2,4)<20 ) & ( ACHI2DOCA(3,4)<20 )" + " & " + PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)

    def applyMVA( self, name, 
                  SelB,
                  MVAVars,
                  MVAxmlFile,
                  MVACutValue
                  ):
        from MVADictHelpers import addTMVAclassifierValue
        from Configurables import FilterDesktop as MVAFilterDesktop

        _FilterB = MVAFilterDesktop( name + "Filter",
                                     Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue  )

        addTMVAclassifierValue( Component = _FilterB,
                                XMLFile   = MVAxmlFile,
                                Variables = MVAVars,
                                ToolName  = name )
        
        return Selection( name,
                          Algorithm =  _FilterB,
                          RequiredSelections = [ SelB ] )