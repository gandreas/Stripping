################################################################################
##                          S T R I P P I N G  2 9                            ##
##                                                                            ##
##  Configuration for B2CC WG                                                 ##
##  Contact person: Xuesong Liu           (xuesong.liu@cern.ch)               ##
################################################################################

from GaudiKernel.SystemOfUnits import *

######################################################################
## StrippingB2JpsiHHBs2Jpsif0PrescaledLine (MicroDST)
## StrippingB2JpsiHHBs2JpsiKstarLin(MicroDST)
## StrippingB2JpsiHHLb2JpsipHLine(MicroDST)
## StrippingB2JpsiHHBs2Jpsif0Line (FullDST)
## StrippingB2JpsiHHBs2Jpsif0KaonLine(FullDST)
## StrippingB2JpsiHHBs2Jpsif0wsLine(FullDST)
## -------------------------------------------------------------------
## Lines defined in StrippingB2JpsiHH.py
## Authors: Liming Zhang, Xuesong Liu
## Last changes made by Xuesong Liu
######################################################################

B2JpsiHH = {
    "BUILDERTYPE": "B2JpsiHHConf",
    "CONFIG"     : {
                   "Bs2Jpsif0Prescale" : 0.3,
			 "DTF_CTAU"		   : 0.0598,
                   "HLTCuts"           : "HLT_PASS_RE('Hlt2DiMuonJPsiDecision')",
                   "JpsiMassWindow"    : 80,
                   "MVACut"            : "-0.1",
                   "TRCHI2DOF"         : 5,
                   "VCHI2PDOF"         : 10,
			 "PROBNNk"		   : 0.05,
			 "PROBNNp"		   : 0.05,
			 "PROBNNpi"		   : 0.05,
			 "PIDKforKaon"	   : 0.0,
			 "PIDKforPion"	   : 10.0,
			 "PIDpforProton"	   : 0.0,
			 "PIDKpforKaon"	   : -10.0,
			 "PIDpKforProton"	   : -10.0,
                   "XmlFile"           : "$TMVAWEIGHTSROOT/data/Bs2Jpsif0_BDT_v1r1.xml"
                   },
    "STREAMS"    : {
                   "Dimuon"   : [
                                "StrippingB2JpsiHHBs2Jpsif0Line",
                                "StrippingB2JpsiHHBs2Jpsif0KaonLine",
                                "StrippingB2JpsiHHBs2Jpsif0wsLine"
                                ],
                   "Leptonic" : [
                                "StrippingB2JpsiHHBs2Jpsif0PrescaledLine",
                                "StrippingB2JpsiHHBs2JpsiKstarLine",
                                "StrippingB2JpsiHHLb2JpsipHLine"
                                ]
                   },
    "WGs": [ "B2CC" ]
    }

######################################################################
## StrippingBetaSBs2JpsieePhiDetachedLine (MicroDST)
## StrippingBetaSBs2JpsieePhiFromTracksLine (MicroDST)
## StrippingBetaSBs2JpsieePhiLine (MicroDST)
## -------------------------------------------------------------------
## Lines defined in StrippingBs2JpsieePhi.py
## Authors: Artur Ukleja, Jibo He, Konrad Klimaszewski
## Last changes made by Konrad Klimaszewski
######################################################################

BetaSBs2JpsieePhi = {
    "BUILDERTYPE": "Bs2JpsieePhiConf",
    "CONFIG"     : {
                   "BsDIRA"                      : 0.99,
                   "BsMassMax"                   : 6000.0,
                   "BsMassMaxLoose"              : 6000.0,
                   "BsMassMin"                   : 3600.0,
                   "BsMassMinLoose"              : 3600.0,
                   "BsVertexCHI2pDOF"            : 10.0,
                   "BsVertexCHI2pDOFLoose"       : 10.0,
                   "ElectronPID"                 : 0.0,
                   "ElectronPIDLoose"            : 0.0,
                   "ElectronPT"                  : 500.0,
                   "ElectronPTLoose"             : 500.0,
                   "ElectronTrackCHI2pDOF"       : 5.0,
                   "ElectronTrackCHI2pDOFLoose"  : 5.0,
                   "JpsiMassMax"                 : 3600.0,
                   "JpsiMassMaxLoose"            : 3600.0,
                   "JpsiMassMin"                 : 1700.0,
                   "JpsiMassMinLoose"            : 1700.0,
                   "JpsiPT"                      : 400.0,
                   "JpsiPTLoose"                 : 400.0,
                   "JpsiVertexCHI2pDOF"          : 15.0,
                   "JpsiVertexCHI2pDOFLoose"     : 15.0,
                   "KaonPID"                     : -3.0,
                   "KaonPIDLoose"                : -3.0,
                   "KaonTrackCHI2pDOF"           : 5.0,
                   "KaonTrackCHI2pDOFLoose"      : 5.0,
                   "LifetimeCut"                 : " & (BPVLTIME()>0.3*ps)",
                   "PhiMassMax"                  : 1050.0,
                   "PhiMassMaxLoose"             : 1050.0,
                   "PhiMassMin"                  : 990.0,
                   "PhiMassMinLoose"             : 990.0,
                   "PhiPT"                       : 1000.0,
                   "PhiPTLoose"                  : 1000.0,
                   "PhiVertexCHI2pDOF"           : 15.0,
                   "PhiVertexCHI2pDOFLoose"      : 15.0,
                   "Prescale"                    : 0.1,
                   "PrescaleLoose"               : 1.0
                   },
    "STREAMS"    : [ "Leptonic" ],
    "WGs"        : [ "B2CC" ]
    }

######################################################################
## StrippingBd2JpsieeKSBd2JpsieeKSFromTracksPrescaledLine (MDST.DST)
## StrippingBd2JpsieeKSBd2JpsieeKSFromTracksDetachedLine (MDST.DST)
## StrippingBd2JpsieeKSBd2JpsieeKSDetachedLine (MDST.DST)
## StrippingBd2JpsieeKSBd2JpsieeKstarFromTracksPrescaledLine (MDST.DST)
## StrippingBd2JpsieeKSBd2JpsieeKstarFromTracksDetachedLine (MDST.DST)
## StrippingBd2JpsieeKSBu2JpsieeKFromTracksDetachedLine (MDST.DST)
## -------------------------------------------------------------------
## Lines defined in StrippingBd2JpsieeKS.py
## Author: Ramon Niet
## Last changes made by Xuesong Liu
######################################################################

Bd2JpsieeKS = {
    "BUILDERTYPE": "Bd2JpsieeKSConf",
    "CONFIG"     : {
                   "BPVLTIME"                 : 0.2,
                   "Bd2JpsieeKstarPrescale"   : 0.1,
                   "BdFromKstarVCHI2pDOF"     : 20.0,
                   "BdMassMax"                : 6000.0,
                   "BdMassMin"                : 4400.0,
                   "BdVertexCHI2pDOF"         : 7.0,
                   "Bu2JpsieeKPrescale"       : 0.1,
                   "ElectronPID"              : 0.0,
                   "ElectronPT"               : 500.0,
                   "ElectronTrackCHI2pDOF"    : 5.0,
                   "JpsiMassMax"              : 3300.0,
                   "JpsiMassMin"              : 2300.0,
                   "JpsiVertexCHI2pDOF"       : 15.0,
                   "KSBPVDLS"                 : 5.0,
                   "KSVCHI2"                  : 20.0,
                   "KaonPID"                  : 0.0,
                   "KplusIP"                  : 9.0,
                   "KplusPT"                  : 800.0,
                   "KstarDaughtersIP"         : 9.0,
                   "KstarMassMax"             : 966.0,
                   "KstarMassMin"             : 826.0,
                   "KstarPT"                  : 1500.0,
                   "KstarVCHI2"               : 20.0,
                   "PionForKstarPT"           : 600.0,
                   "Prescale"                 : 0.1,
                   "TRCHI2DOF"                : 3.0
                   },
    "STREAMS"    : {
                   "Dimuon"  : [
                               "StrippingBd2JpsieeKSBd2JpsieeKstarFromTracksDetachedLine",
                               "StrippingBd2JpsieeKSBu2JpsieeKFromTracksDetachedLine"
                               ],
                   "Leptonic": [
                               "StrippingBd2JpsieeKSBd2JpsieeKSFromTracksPrescaledLine",
                               "StrippingBd2JpsieeKSBd2JpsieeKstarFromTracksPrescaledLine",
                               "StrippingBd2JpsieeKSBd2JpsieeKSFromTracksDetachedLine",
                               "StrippingBd2JpsieeKSBd2JpsieeKSDetachedLine"
                               ]
                   },
    "WGs"        : [ "B2CC" ]
    }

######################################################################
## StrippingBetaSBd2JpsiPi0DetachedLine (FullDST)
## -------------------------------------------------------------------
## Lines defined in StrippingB2JpsiPi0.py
## Authors: Maximilien Chefdeville
######################################################################

BetaSPi0 = {
    "BUILDERTYPE"         : "B2JpsiPi0Conf",
    "CONFIG"              : { "BPVLTIME": 0.2,
                              "JpsiMassWindow": 80,
                              "VCHI2PDOF": 10
                            },
    "STREAMS"             : { "Dimuon": [ "StrippingBetaSBd2JpsiPi0DetachedLine" ] },
    "WGs"                 : [ "B2CC" ]
           }

######################################################################
## StrippingBs2EtacPhiBDTLine (MicroDST)
## StrippingBs2EtacPhiBDT_KsKPiLine (MicroDST)
## -------------------------------------------------------------------
## Lines defined in StrippingBs2EtacPhiBDT.py
## Authors: Morgan Martin, Jibo He
## Last changes made by Jibo He
######################################################################

Bs2EtacPhiBDT = {
      "BUILDERTYPE"     : "Bs2EtacPhiBDTConf",
      "CONFIG"          : {  "Bs2EtacPhiMVACut"       : "-0.1",
                             "Bs2EtacPhiXmlFile"      : "$TMVAWEIGHTSROOT/data/Bs2EtacPhi_BDT_v1r0.xml",
                             "BsComCuts"              : "(ADAMASS('B_s0') < 500 *MeV)",
                             "BsMomCuts"              : "(VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.99) & (BPVIPCHI2()<25) & (BPVDLS>0)",
                             "EtacComAMCuts"          : "(AM<3.25*GeV)",
                             "EtacComCuts"            : "(in_range(2.75*GeV, AM, 3.25*GeV))",
                             "EtacComN4Cuts"          : "(in_range(2.75*GeV, AM, 3.25*GeV)) & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.5 *GeV) & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>30) ",
                             "EtacMomN4Cuts"          : "(VFASPF(VCHI2/VDOF) < 9.) & (in_range(2.8*GeV, MM, 3.2*GeV)) & (MIPCHI2DV(PRIMARY) > 2.)",
                             "HLTCuts"                : "(HLT_PASS_RE('Hlt2Topo.*Decision') | HLT_PASS_RE('Hlt2(Phi)?IncPhi.*Decision'))",
                             "KaonCuts"               : "(PROBNNk > 0.13) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
                             "KsCuts"                 : "(ADMASS('KS0') < 30.*MeV) & (BPVDLS>5)",
                             "PhiCuts"                : "(MAXTREE(ABSID=='K+',TRGHOSTPROB) < 0.4) & (MM<1.05*GeV)& (PT> 800*MeV)& (MIPCHI2DV(PRIMARY) > 2.) & (VFASPF(VCHI2) < 9.)",
                             "PionCuts"               : "(PROBNNpi > 0.2) & (PT > 250*MeV) & (TRGHOSTPROB<0.4)",
                             "Prescale"               : 1.0
                          },
      "STREAMS"         : [ "Bhadron" ],
      "WGs"             : [ "B2CC" ]
                }

#############################################################################################
## StrippingBetaSBu2JpsiKDetachedLine (FullDST)
## StrippingBetaSBd2JpsiKstarDetachedLine (FullDST)
## StrippingBetaSBs2JpsiPhiDetachedLine (FullDST)
## StrippingBetaSBd2JpsiKsDetachedLine (FullDST)
## StrippingBetaSJpsi2MuMuLine (FullDST)
## StrippingBetaSLambdab2JpsiLambdaUnbiasedLine (MicroDST)
## StrippingBetaSBu2JpsiKPrescaledLine (MicroDST)
## StrippingBetaSBs2JpsiPhiPrescaledLine (MicroDST)
## StrippingBetaSBd2JpsiKstarPrescaledLine (MicroDST)
## StrippingBetaSBd2JpsiKsPrescaledLine (MicroDST)
## StrippingBetaSBd2JpsiKsLDDetachedLine (MicroDST)
## StrippingBetaSBs2JpsiKstarWideLine (MicroDST)
## ----------------------------------------------------------------------------
## Lines defined in StrippingB2JpsiXforBeta_s.py
## Authors: Greig Cowan, Juan Palacios, Francesca Dordei, Carlos Vazquez Sierra, Xuesong Liu
## Last changes made by Xuesong Liu
#############################################################################################

BetaS = {
        "BUILDERTYPE": "B2JpsiXforBeta_sConf",
        "CONFIG"     : { "DTF_CTAU": 0.0598,
                         "Bd2JpsiKsPrescale": 1.0,
                         "Bd2JpsiKstarPrescale": 1.0,
                         "Bs2JpsiPhiPrescale": 1.0,
                         "Bu2JpsiKPrescale": 1.0,
                         "DaughterPT": 1000,
                         "HLTCuts": "HLT_PASS_RE('Hlt2DiMuonJPsiDecision')",
                         "Jpsi2MuMuPrescale": 0.04,
                         "JpsiMassWindow": 80,
				 "PIDKCuts": 0.0,
				 "PIDpiCuts": 0.0,
                         "TRCHI2DOF": 5,
                         "VCHI2PDOF": 10
                       },
        "STREAMS"    : { "Dimuon"   : [ "StrippingBetaSBu2JpsiKDetachedLine",
                                        "StrippingBetaSBd2JpsiKstarDetachedLine",
                                        "StrippingBetaSBs2JpsiPhiDetachedLine",
                                        "StrippingBetaSJpsi2MuMuLine",
                                        "StrippingBetaSBd2JpsiKsDetachedLine"
                                      ],
                         "Leptonic" : [ "StrippingBetaSBu2JpsiKPrescaledLine",
                                        "StrippingBetaSBs2JpsiPhiPrescaledLine",
                                        "StrippingBetaSBd2JpsiKstarPrescaledLine",
                                        "StrippingBetaSBd2JpsiKsPrescaledLine",
                                        "StrippingBetaSBd2JpsiKsLDDetachedLine",
                                        "StrippingBetaSBs2JpsiKstarWideLine",
                                        "StrippingBetaSLambdab2JpsiLambdaUnbiasedLine"
                                      ]
                       },
        "WGs"        : [ "B2CC" ]
        }
