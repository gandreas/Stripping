import re
import sys
import json
from StrippingSelections import buildersConf

def get_entry(name, config) :
    config.pop('NAME', None) # Strangely, some conf doesn't have 'NAME' 
    val   = json.dumps( config, sort_keys=True, indent=4 )
    val   = re.sub(r'\[\n\s+(\S+)\n\s+\]', r'[ \1 ]', val) # Pretty one-liner list
    val   = re.sub(r'false', 'False', val) # Capitalize boolean patch
    val   = re.sub(r'true', 'True', val) 
    val   = re.sub(r'null', 'None', val) # null-value patch
    entry = name + ' = ' + val + '\n\n'
    return entry

def gen_dict(wg, builders = None, fname = None) :
    if not fname :
        fname = 'LineConfigDictionaries_' + wg + '.py'
    confs = buildersConf(WGs=[wg])
    if builders :
        confs = dict((name, conf) for name, conf in confs.iteritems() if name in builders)
    with open(fname, 'w') as fout :
        for name, config in sorted(confs.iteritems()) :
            fout.write(get_entry(name, config))
            print 'Wrote:', name
    print 'Successfully wrote:', fname
        
