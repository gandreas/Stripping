#!/bin/bash

# Test the Stripping options files in StrippingCache.

if [ $# != 2 ] ; then
    echo 'Usage : ./test-strippingcache-opts.sh <DaVinciDev dir> <StrippingCache opts>'
    echo 'eg: ./test-strippingcache-opts.sh ./DaVinciDev_v41r3 $DaVinci_release_area/DAVINCI/DAVINCI_v41r3/Phys/StrippingCache/options'
    echo "If the <StrippingCache opts> is a directory it tests all the options in there, else it can be a file and it'll only run that file."
    exit 1
fi

dvdir=$1
cacheopts=$2
if [ -d $cacheopts ] ; then
    optsfiles=$(ls $cacheopts/DV-Stripping*-Stripping.py)
else 
    optsfiles=$cacheopts
fi

for f in $optsfiles ; do
    sbase=$(basename $f)
    $dvdir/run gaudirun.py -n -o $sbase.pkl $f $cacheopts/DisableLoKiCacheFunctors.py /afs/cern.ch/lhcb/software/releases/LHCB/LHCB_v42r0/InstallArea/x86_64-slc6-gcc49-opt/cmake/LoKiFunctorsCachePostActionOpts.py >& stdout-$sbase
    if [ $? = 0 ] ; then
	echo "$sbase option pickling is OK."
    else 
	echo "$sbase option pickling failed! See stdout-$sbase"
    fi
    $dvdir/run LOKI_GENERATE_CPPCODE=50 LOKI_DISABLE_CACHE=1 gaudirun.py $sbase.pkl >& stdout-$sbase.pkl
    if [ $? = 0 ] ; then
	echo "$sbase functor cache generation is OK."
    else 
	echo "$sbase functor cache generation failed! See stdout-$sbase.pkl"
    fi
done
