#!/bin/env python

import sys, subprocess
from DIRAC.Core.Base.Script import parseCommandLine
parseCommandLine() # Need this for some reason.
from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequest import ProductionRequest

def step_info(stepid) :
    pr = ProductionRequest()
    pr.stepsList = [stepid]
    pr.resolveSteps()
    return pr.stepsListDict[0]

if __name__ == '__main__' :
    stepinfo = step_info(int(sys.argv[1]))
    print 'dvversion=' + stepinfo['ApplicationVersion']
    print 'conddb=' + stepinfo['CONDDB']
    print 'dddb=' + stepinfo['DDDB']
    opts = filter(lambda opt : 'Stripping' in opt, stepinfo['OptionFiles'].split(';'))[0]
    print 'opts=' + opts.strip().replace('$', '\\$')
    platform = stepinfo['SystemConfig']
    if platform == 'NULL' or not platform :
        proc = subprocess.Popen(('lb-sdb-query listPlatforms DaVinci ' + stepinfo['ApplicationVersion']).split(),
                                stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        stdout, stderr = proc.communicate()
        platform = filter(lambda plat : all(test in plat for test in ('x86_64', 'slc', 'gcc', 'opt')), 
                          stdout.split('\n'))
        if platform :
            platform = platform[0]
        else :
            platform = ''
    print 'platform=' + platform
